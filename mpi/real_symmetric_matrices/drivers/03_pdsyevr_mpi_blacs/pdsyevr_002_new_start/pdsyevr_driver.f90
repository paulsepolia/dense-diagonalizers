!===============================================================================
!
!      SUBROUTINE PDSYEVR( JOBZ, RANGE, UPLO, N, A, IA, JA, 
!     $                    DESCA, VL, VU, IL, IU, M, NZ, W, Z, IZ,
!     $                    JZ, DESCZ, WORK, LWORK, IWORK, LIWORK,
!     $                    INFO )
!
!      IMPLICIT NONE
!
!  -- ScaLAPACK routine (version 2.0.2) --
!     Univ. of Tennessee, Univ. of California Berkeley, Univ. of Colorado Denver
!     May 1 2012
!
!     .. Scalar Arguments ..
!      CHARACTER          JOBZ, RANGE, UPLO
!      INTEGER            IA, IL, INFO, IU, IZ, JA, JZ, LIWORK, LWORK, M,
!     $                   N, NZ
!      DOUBLE PRECISION VL, VU
!     ..
!     .. Array Arguments ..
!      INTEGER            DESCA( * ), DESCZ( * ), IWORK( * )
!      DOUBLE PRECISION   A( * ), W( * ), WORK( * ), Z( * )
!     ..
!
!  Purpose
!  =======
!
!  PDSYEVR computes selected eigenvalues and, optionally, eigenvectors
!  of a real symmetric matrix A distributed in 2D blockcyclic format
!  by calling the recommended sequence of ScaLAPACK routines.  
!
!  First, the matrix A is reduced to real symmetric tridiagonal form.
!  Then, the eigenproblem is solved using the parallel MRRR algorithm.
!  Last, if eigenvectors have been computed, a backtransformation is done.
!
!  Upon successful completion, each processor stores a copy of all computed
!  eigenvalues in W. The eigenvector matrix Z is stored in 
!  2D blockcyclic format distributed over all processors.
!
!  Note that subsets of eigenvalues/vectors can be selected by
!  specifying a range of values or a range of indices for the desired
!  eigenvalues.
!
!  For constructive feedback and comments, please contact cvoemel@lbl.gov
!  C. Voemel
!
!  Arguments
!  =========
!
!  JOBZ    (global input) CHARACTER*1
!          Specifies whether or not to compute the eigenvectors:
!          = 'N':  Compute eigenvalues only.
!          = 'V':  Compute eigenvalues and eigenvectors.
!
!  RANGE   (global input) CHARACTER*1
!          = 'A': all eigenvalues will be found.
!          = 'V': all eigenvalues in the interval [VL,VU] will be found.
!          = 'I': the IL-th through IU-th eigenvalues will be found.
!
!  UPLO    (global input) CHARACTER*1
!          Specifies whether the upper or lower triangular part of the
!          symmetric matrix A is stored:
!          = 'U':  Upper triangular
!          = 'L':  Lower triangular
!
!  N       (global input) INTEGER
!          The number of rows and columns of the matrix A.  N >= 0
!
!  A       (local input/workspace) 2D block cyclic DOUBLE PRECISION array,
!          global dimension (N, N),
!          local dimension ( LLD_A, LOCc(JA+N-1) ),
!          (see Notes below for more detailed explanation of 2d arrays)  
!
!          On entry, the symmetric matrix A.  If UPLO = 'U', only the
!          upper triangular part of A is used to define the elements of
!          the symmetric matrix.  If UPLO = 'L', only the lower
!          triangular part of A is used to define the elements of the
!          symmetric matrix.
!
!          On exit, the lower triangle (if UPLO='L') or the upper
!          triangle (if UPLO='U') of A, including the diagonal, is
!          destroyed.
!
!  IA      (global input) INTEGER
!          A's global row index, which points to the beginning of the
!          submatrix which is to be operated on. 
!          It should be set to 1 when operating on a full matrix.
!
!  JA      (global input) INTEGER
!          A's global column index, which points to the beginning of
!          the submatrix which is to be operated on.
!          It should be set to 1 when operating on a full matrix.
!
!  DESCA   (global and local input) INTEGER array of dimension DLEN=9.
!          The array descriptor for the distributed matrix A.
!          The descriptor stores details about the 2D block-cyclic 
!          storage, see the notes below.
!          If DESCA is incorrect, PDSYEVR cannot guarantee
!          correct error reporting.
!          Also note the array alignment requirements specified below.
!
!  VL      (global input) DOUBLE PRECISION 
!          If RANGE='V', the lower bound of the interval to be searched
!          for eigenvalues.  Not referenced if RANGE = 'A' or 'I'.
!
!  VU      (global input) DOUBLE PRECISION 
!          If RANGE='V', the upper bound of the interval to be searched
!          for eigenvalues.  Not referenced if RANGE = 'A' or 'I'.
!
!  IL      (global input) INTEGER
!          If RANGE='I', the index (from smallest to largest) of the
!          smallest eigenvalue to be returned.  IL >= 1.
!          Not referenced if RANGE = 'A'.
!
!  IU      (global input) INTEGER
!          If RANGE='I', the index (from smallest to largest) of the
!          largest eigenvalue to be returned.  min(IL,N) <= IU <= N.
!          Not referenced if RANGE = 'A'.
!
!  M       (global output) INTEGER
!          Total number of eigenvalues found.  0 <= M <= N.
!
!  NZ      (global output) INTEGER
!          Total number of eigenvectors computed.  0 <= NZ <= M.
!          The number of columns of Z that are filled.
!          If JOBZ .NE. 'V', NZ is not referenced.
!          If JOBZ .EQ. 'V', NZ = M 
!
!  W       (global output) DOUBLE PRECISION array, dimension (N)
!          Upon successful exit, the first M entries contain the selected
!          eigenvalues in ascending order.
!
!  Z       (local output) DOUBLE PRECISION array,
!          global dimension (N, N),
!          local dimension ( LLD_Z, LOCc(JZ+N-1) )
!          (see Notes below for more detailed explanation of 2d arrays)  
!          If JOBZ = 'V', then on normal exit the first M columns of Z
!          contain the orthonormal eigenvectors of the matrix
!          corresponding to the selected eigenvalues.
!          If JOBZ = 'N', then Z is not referenced.
!
!  IZ      (global input) INTEGER
!          Z's global row index, which points to the beginning of the
!          submatrix which is to be operated on.
!          It should be set to 1 when operating on a full matrix.
!
!  JZ      (global input) INTEGER
!          Z's global column index, which points to the beginning of
!          the submatrix which is to be operated on.
!          It should be set to 1 when operating on a full matrix.
!
!  DESCZ   (global and local input) INTEGER array of dimension DLEN_.
!          The array descriptor for the distributed matrix Z.
!          The context DESCZ( CTXT_ ) must equal DESCA( CTXT_ ).
!          Also note the array alignment requirements specified below.
!
!  WORK    (local workspace/output) DOUBLE PRECISION  array,
!          dimension (LWORK)
!          On return, WORK(1) contains the optimal amount of
!          workspace required for efficient execution.
!          if JOBZ='N' WORK(1) = optimal amount of workspace
!             required to compute the eigenvalues.
!          if JOBZ='V' WORK(1) = optimal amount of workspace
!             required to compute eigenvalues and eigenvectors.
!
!  LWORK   (local input) INTEGER
!          Size of WORK, must be at least 3.
!          See below for definitions of variables used to define LWORK.
!          If no eigenvectors are requested (JOBZ = 'N') then
!             LWORK >= 2 + 5*N + MAX( 12 * NN, NB * ( NP0 + 1 ) )
!          If eigenvectors are requested (JOBZ = 'V' ) then
!             the amount of workspace required is:
!             LWORK >= 2 + 5*N + MAX( 18*NN, NP0 * MQ0 + 2 * NB * NB ) +
!               (2 + ICEIL( NEIG, NPROW*NPCOL))*NN
!
!          Variable definitions:
!             NEIG = number of eigenvectors requested
!             NB = DESCA( MB_ ) = DESCA( NB_ ) =
!                  DESCZ( MB_ ) = DESCZ( NB_ )
!             NN = MAX( N, NB, 2 )
!             DESCA( RSRC_ ) = DESCA( NB_ ) = DESCZ( RSRC_ ) =
!                              DESCZ( CSRC_ ) = 0
!             NP0 = NUMROC( NN, NB, 0, 0, NPROW )
!             MQ0 = NUMROC( MAX( NEIG, NB, 2 ), NB, 0, 0, NPCOL )
!             ICEIL( X, Y ) is a ScaLAPACK function returning
!             ceiling(X/Y)
!
!          If LWORK = -1, then LWORK is global input and a workspace
!          query is assumed; the routine only calculates the size
!          required for optimal performance for all work arrays. Each of
!          these values is returned in the first entry of the
!          corresponding work arrays, and no error message is issued by
!          PXERBLA.
!          Note that in a workspace query, for performance the optimal 
!          workspace LWOPT is returned rather than the minimum necessary 
!          WORKSPACE LWMIN. For very small matrices, LWOPT >> LWMIN.
!
!  IWORK   (local workspace) INTEGER array
!          On return, IWORK(1) contains the amount of integer workspace
!          required.
!
!  LIWORK  (local input) INTEGER
!          size of IWORK
!
!          Let  NNP = MAX( N, NPROW*NPCOL + 1, 4 ). Then:
!          LIWORK >= 12*NNP + 2*N when the eigenvectors are desired
!          LIWORK >= 10*NNP + 2*N when only the eigenvalues have to be computed
!          
!          If LIWORK = -1, then LIWORK is global input and a workspace
!          query is assumed; the routine only calculates the minimum
!          and optimal size for all work arrays. Each of these
!          values is returned in the first entry of the corresponding
!          work array, and no error message is issued by PXERBLA.
!
!  INFO    (global output) INTEGER
!          = 0:  successful exit
!          < 0:  If the i-th argument is an array and the j-entry had
!                an illegal value, then INFO = -(i*100+j), if the i-th
!                argument is a scalar and had an illegal value, then
!                INFO = -i.
!
!  Notes
!  =====
!
!  Each global data object is described by an associated description
!  vector.  This vector stores the information required to establish
!  the mapping between an object element and its corresponding process
!  and memory location.
!
!  Let A be a generic term for any 2D block cyclicly distributed array.
!  Such a global array has an associated description vector DESCA, 
!  or DESCZ for the descriptor of Z, etc. 
!  The length of a ScaLAPACK descriptor is nine.
!  In the following comments, the character _ should be read as
!  "of the global array".
!
!  NOTATION        STORED IN      EXPLANATION
!  --------------- -------------- --------------------------------------
!  DTYPE_A(global) DESCA( DTYPE_ )The descriptor type.  In this case,
!                                 DTYPE_A = 1.
!  CTXT_A (global) DESCA( CTXT_ ) The BLACS context handle, indicating
!                                 the BLACS process grid A is distribu-
!                                 ted over. The context itself is glo-
!                                 bal, but the handle (the integer
!                                 value) may vary.
!  M_A    (global) DESCA( M_ )    The number of rows in the global
!                                 array A.
!  N_A    (global) DESCA( N_ )    The number of columns in the global
!                                 array A.
!  MB_A   (global) DESCA( MB_ )   The blocking factor used to distribute
!                                 the rows of the array.
!  NB_A   (global) DESCA( NB_ )   The blocking factor used to distribute
!                                 the columns of the array.
!  RSRC_A (global) DESCA( RSRC_ ) The process row over which the first
!                                 row of the array A is distributed.
!  CSRC_A (global) DESCA( CSRC_ ) The process column over which the
!                                 first column of the array A is
!                                 distributed.
!  LLD_A  (local)  DESCA( LLD_ )  The leading dimension of the local
!                                 array.  LLD_A >= MAX(1,LOCr(M_A)).
!
!  Let K be the number of rows or columns of a distributed matrix,
!  and assume that its process grid has dimension p x q.
!  LOCr( K ) denotes the number of elements of K that a process
!  would receive if K were distributed over the p processes of its
!  process column.
!  Similarly, LOCc( K ) denotes the number of elements of K that a
!  process would receive if K were distributed over the q processes of
!  its process row.
!  The values of LOCr() and LOCc() may be determined via a call to the
!  ScaLAPACK tool function, NUMROC:
!          LOCr( M ) = NUMROC( M, MB_A, MYROW, RSRC_A, NPROW ),
!          LOCc( N ) = NUMROC( N, NB_A, MYCOL, CSRC_A, NPCOL ).
!  An upper bound for these quantities may be computed by:
!          LOCr( M ) <= ceil( ceil(M/MB_A)/NPROW )*MB_A
!          LOCc( N ) <= ceil( ceil(N/NB_A)/NPCOL )*NB_A
!
!  PDSYEVR assumes IEEE 754 standard compliant arithmetic. 
!
!  Alignment requirements
!  ======================
!
!  The distributed submatrices A(IA:*, JA:*) and Z(IZ:IZ+M-1,JZ:JZ+N-1)
!  must satisfy the following alignment properties:
!
!  1.Identical (quadratic) dimension: 
!    DESCA(M_) = DESCZ(M_) = DESCA(N_) = DESCZ(N_)
!  2.Quadratic conformal blocking: 
!    DESCA(MB_) = DESCA(NB_) = DESCZ(MB_) = DESCZ(NB_)
!    DESCA(RSRC_) = DESCZ(RSRC_)
!  3.MOD( IA-1, MB_A ) = MOD( IZ-1, MB_Z ) = 0
!  4.IAROW = IZROW
!
!===============================================================================

  program pdsyevr_driver_mpi

  use mpi
  use m_1_type_definitions

  implicit none

  !  1. mpi and blacs related variables

  integer(kind=si) :: ierr        ! mpi
  integer(kind=si) :: comm        ! mpi
  integer(kind=si) :: my_rank     ! blacs
  integer(kind=si) :: my_rank_mpi ! mpi
  integer(kind=si) :: n_procs     ! mpi + blacs
  integer(kind=si) :: ctxt        ! blacs
  integer(kind=si) :: my_row      ! blacs
  integer(kind=si) :: my_col      ! blacs
  integer(kind=si) :: np_row      ! blacs
  integer(kind=si) :: np_col      ! blacs
  integer(kind=si) :: n_loc_row   ! blacs
  integer(kind=si) :: n_loc_col   ! blacs
  integer(kind=si) :: numroc      ! blacs routine

  !  2. scalapack routine related variables

  character        :: jobz
  character        :: uplo
  integer(kind=si) :: info
  integer(kind=si) :: lwork
  integer(kind=si) :: liwork
  integer(kind=si) :: n

  integer(kind=si) :: ia
  integer(kind=si) :: ja
  integer(kind=si) :: iz
  integer(kind=si) :: jz

  real(kind=dr), allocatable, dimension(:,:) :: a
  real(kind=dr), allocatable, dimension(:,:) :: z
  real(kind=dr), allocatable, dimension(:)   :: w
  real(kind=dr), allocatable, dimension(:)   :: work
  real(kind=dr), allocatable, dimension(:)   :: iwork

  integer(kind=si), allocatable, dimension(:) :: desca
  integer(kind=si), allocatable, dimension(:) :: descz

  !  3. local variables

  integer(kind=si) :: i
  integer(kind=si) :: j
  integer(kind=si) :: i_local
  integer(kind=si) :: j_local
  integer(kind=si) :: coef
  integer(kind=si) :: np
  integer(kind=si) :: nq
  integer(kind=si) :: trilwmin

  !  4. setting the variables

  n      = 32000
  coef   = 2000
  np_row = 16
  np_col = 1 
  jobz   = 'V'
  uplo   = 'U'
  ia     = 1
  ja     = 1
  iz     = 1
  jz     = 1
 
  !  5. mpi start up

  call mpi_init(ierr)

  comm = mpi_comm_world

  call mpi_comm_rank(comm, my_rank_mpi, ierr)

  !  6. blacs start up

  call blacs_pinfo(my_rank, n_procs) ! to get 'my_rank' and 'n_procs'

  call sl_init(ctxt, np_row, np_col)

  call blacs_gridinfo(ctxt, np_row, np_col, my_row, my_col)

  call mpi_barrier(comm, ierr)

  !  7. initialization of the interface variables

  n_loc_row = n / np_row
  n_loc_col = n / np_col

  trilwmin = 3*n + max(coef*(coef+1), 3*coef)
  np = numroc(n, coef, my_row, my_rank, np_row)
  nq = numroc(n, coef, my_col, my_rank, np_col)
  lwork = max(1+6*n+2*coef*n, trilwmin) + 2*n + 10

  liwork = 7*n + 8*np_col + 2

  !  8. allocation of the workspace

  allocate(a(1:n_loc_row,1:n_loc_col))
  allocate(w(1:n))
  allocate(work(1:lwork))
  allocate(iwork(1:liwork))
  allocate(z(1:n_loc_row,1:n_loc_col))

  z = 0.0_dr
  a = 0.0_dr

  !  9. building a test the matrix

  do i = 1, n_loc_row
    do j = 1, n_loc_col

      i_local = i + my_row * n_loc_row
      j_local = j + my_col * n_loc_col

      if (abs(i_local - j_local) < 5) then
        a(i,j) = i_local + j_local
      else if (abs(i_local - j_local) >= 5) then
        a(i,j) = 0.0_dr
      end if

     end do
   end do

   call mpi_barrier(comm, ierr)

  ! 10. the main mpi diagonalization step

  allocate(desca(1:9))
  allocate(descz(1:9))  
  
  call descinit(desca, n, n, coef, coef, 0, 0, ctxt, coef, info)

  call descinit(descz, n, n, coef, coef, 0, 0, ctxt, coef, info)
  
  call mpi_barrier(comm, ierr)

  call pdsyevr( jobz,     &   !  1.
                uplo,     &   !  2.
                   n,     &   !  3.
                   a,     &   !  4.
                  ia,     &   !  5.
                  ja,     &   !  6.
               desca,     &   !  7.
                   w,     &   !  8.
                   z,     &   !  9.
                  iz,     &   ! 10.
                  jz,     &   ! 11.
               descz,     &   ! 12.
                work,     &   ! 13.
               lwork,     &   ! 14.
               iwork,     &   ! 15.
              liwork,     &   ! 16.
               info )         ! 17.
     
  ! 11. write down the info. if the job was successful or not
  
  if (my_rank == 0) then
  
    write(*,*) info
  
  end if

  ! 12. write in to the hard disk the eigevalues in asceding order

  if (my_rank == 0) then

    do i = 1, n

      write(*,*) i, w(i)

    end do
  
  end if

  ! 13. exiting blacs grid and finalize mpi here

  call blacs_gridexit(ctxt)
  call blacs_exit(0)

  end program pdsyevr_driver_mpi

!======!
! FINI !
!======!
