#!/bin/bash

# 1. compiling. 

  mpiifort -O3 -static-intel       \
  -mkl=cluster                     \
  m_1_type_definitions.f90         \
  pdsyevr_driver.f90               \
  pdsyevr_scalapack_v202.f         \
  -o x_mpi_scalapack_mkl

# 2. cleaning.

  rm *.mod

# 3. exiting.
