#!/bin/bash

# 1. compiling. 

  mpiifort -O3 -static_intel                \
  -mkl=cluster                              \
  m_1_type_definitions.f90                  \
  pdsyevr_driver.f90                        \
  pdsyevr_scalapack_v202.f                  \
  -L/opt/acml_520/ifort64_mp/lib -lacml_mp  \
  -o x_mpi_scalapack_acml

# 2. cleaning.

  rm *.mod

# 3. exiting.
