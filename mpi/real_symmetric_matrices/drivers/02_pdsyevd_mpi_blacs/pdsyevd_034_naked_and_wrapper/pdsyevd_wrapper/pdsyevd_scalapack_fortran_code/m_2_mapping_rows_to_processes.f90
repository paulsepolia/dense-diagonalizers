!=======================================!
! Code to map rows to mpi processes     !
!=======================================!
! Date: 2012-12-13                      !
! Author: Pavlos G. Galiatsatos         !
!=======================================! 

  module m_2_mapping_rows_to_processes

  use m_1_type_definitions, only: dr, sr, di, si
  use mpi

  implicit none

  contains

  subroutine mapping_rows_to_processes( n,                        &   !   1. 
                                        mb,                       &   !   2.
                                        nb,                       &   !   3.
                                        np_row,                   &   !   4.
                                        comm,                     &   !   5.
                                        ctxt,                     &   !   6.
                                        box_size,                 &   !   7.
                                        box_num_full,             &   !   8.
                                        box_size_last,            &   !   9.
                                        procs_box_last_full,      &   !  10.
                                        procs_row_alpha,          &   !  11.
                                        procs_row_beta,           &   !  12.
                                        procs_row_gamma,          &   !  13.
                                        num_procs_row_alpha,      &   !  14.
                                        num_procs_row_beta,       &   !  15.
                                        num_procs_row_gamma,      &   !  16.
                                        my_row,                   &   !  17.
                                        a,                        &   !  18.
                                        z,                        &   !  19.
                                        desca,                    &   !  20.
                                        descz )                       !  21.

  implicit none

  !  1. interface variables

  integer(kind=si),                            intent(in)  :: n                     !   1.
  integer(kind=si),                            intent(in)  :: mb                    !   2.
  integer(kind=si),                            intent(in)  :: nb                    !   3.
  integer(kind=si),                            intent(in)  :: np_row                !   4.
  integer(kind=si),                            intent(in)  :: comm                  !   5.
  integer(kind=si),                            intent(in)  :: ctxt                  !   6.
  integer(kind=si),                            intent(out) :: box_size              !   7.
  integer(kind=si),                            intent(out) :: box_num_full          !   8.
  integer(kind=si),                            intent(out) :: box_size_last         !   9.
  integer(kind=si),                            intent(out) :: procs_box_last_full   !  10.
  integer(kind=si),                            intent(out) :: procs_row_alpha       !  11.
  integer(kind=si),                            intent(out) :: procs_row_beta        !  12.
  integer(kind=si),                            intent(out) :: procs_row_gamma       !  13.
  integer(kind=si), allocatable, dimension(:), intent(out) :: num_procs_row_alpha   !  14.
  integer(kind=si),                            intent(out) :: num_procs_row_beta    !  15.
  integer(kind=si), allocatable, dimension(:), intent(out) :: num_procs_row_gamma   !  16.
  integer(kind=si),                            intent(in)  :: my_row                !  17.
  real(kind=dr), allocatable, dimension(:,:),  intent(out) :: a                     !  18.
  real(kind=dr), allocatable, dimension(:,:),  intent(out) :: z                     !  19.
  integer(kind=si), allocatable, dimension(:), intent(out) :: desca                 !  20.
  integer(kind=si), allocatable, dimension(:), intent(out) :: descz                 !  21.

  !  2. local variables

  integer(kind=si) :: n_loc_row
  integer(kind=si) :: n_loc_col
  integer(kind=si) :: lead_dim
  integer(kind=si) :: ierr
  integer(kind=si) :: i
  integer(kind=si) :: info

  !  3. the size of the box

  a = 0.0_dr
  z = 0.0_dr

  allocate(desca(1:9))
  allocate(descz(1:9))
 
  box_size = np_row * mb

  if (box_size > n) then
  
    write(*,*) " box_size > n ==> stop. adjust the 'np_row' and/or 'mb' "
 
    stop    
 
  end if

  call mpi_barrier(comm, ierr)

  !  4. how many boxes are full of small boxes <==> mpi process 

  box_num_full = n / box_size

  !  5. what is the size of the last non-full box

  box_size_last = mod(n, box_size)

  !  6. how many processes are full in the last box

  procs_box_last_full = box_size_last / mb

  !  7. how many processes are of type: alpha 

  procs_row_alpha = procs_box_last_full

  call mpi_barrier(comm, ierr)

  !  8. about processes and rows of type: alpha

  if (procs_row_alpha /= 0) then

    allocate(num_procs_row_alpha(1:procs_row_alpha))

    do i = 1, procs_row_alpha

      num_procs_row_alpha(i) = i-1

    end do

  end if

  !  9. about processes and rows of type: beta

  if (mod(box_size_last,mb) == 0) then
 
    procs_row_beta = 0 

  else 
   
    procs_row_beta = 1

    num_procs_row_beta = procs_row_alpha

  end if

  ! 10. about processes and rows of type: gamma

  procs_row_gamma = np_row - (procs_row_alpha + procs_row_beta)

  if (procs_row_gamma /= 0) then

    allocate(num_procs_row_gamma(1: procs_row_gamma))

    do i = 1, procs_row_gamma

      num_procs_row_gamma(i) = i-1 + procs_row_alpha + procs_row_beta

    end do

  end if

  call mpi_barrier(comm, ierr)

  ! 11. mapping partitioned matrix to process grid

  ! 12. --> part --> alpha

  do i = 1, procs_row_alpha

    if (my_row == num_procs_row_alpha(i)) then

      n_loc_row = (box_num_full + 1)* mb
      n_loc_col = n
      lead_dim  = n_loc_row

      allocate(a(1:n_loc_row,1:n_loc_col))
      allocate(z(1:n_loc_row,1:n_loc_col))

      call descinit(desca, n, n, mb, nb, 0, 0, ctxt, lead_dim, info)
      call descinit(descz, n, n, mb, nb, 0, 0, ctxt, lead_dim, info)

    end if

  end do

  call mpi_barrier(comm, ierr)

  ! 13. --> part --> beta

  do i = 1, procs_row_beta

    if (my_row == procs_row_alpha .and. mod(box_size_last, mb) /=0 ) then

      n_loc_row = box_num_full * mb + mod(box_size_last, mb)
      n_loc_col = n
      lead_dim  = n_loc_row

      allocate(a(1:n_loc_row,1:n_loc_col))
      allocate(z(1:n_loc_row,1:n_loc_col))

      call descinit(desca, n, n, mb, nb, 0, 0, ctxt, lead_dim, info)
      call descinit(descz, n, n, mb, nb, 0, 0, ctxt, lead_dim, info)

    end if

  end do

  call mpi_barrier(comm, ierr)

  ! 14. --> part --> gamma

  do i = 1, procs_row_gamma

    if (my_row == num_procs_row_gamma(i)) then

      n_loc_row = box_num_full * mb 
      n_loc_col = n
      lead_dim  = n_loc_row

      allocate(a(1:n_loc_row,1:n_loc_col))
      allocate(z(1:n_loc_row,1:n_loc_col))

      call descinit(desca, n, n, mb, nb, 0, 0, ctxt, lead_dim, info)
      call descinit(descz, n, n, mb, nb, 0, 0, ctxt, lead_dim, info)

    end if

  end do

  call mpi_barrier(comm, ierr)

  end subroutine mapping_rows_to_processes

  end module m_2_mapping_rows_to_processes

!==============!
! end of code. !
!==============!
