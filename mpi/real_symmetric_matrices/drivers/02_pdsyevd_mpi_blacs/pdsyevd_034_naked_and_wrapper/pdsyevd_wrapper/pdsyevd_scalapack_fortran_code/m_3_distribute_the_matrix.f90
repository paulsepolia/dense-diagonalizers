!============================================================!
! Code to distribute the global matrix to local sub-matrices !
!============================================================!
! Date: 2012-12-13                                           !
! Author: Pavlos G. Galiatsatos                              !
!============================================================! 

  module m_3_distribute_the_matrix

  use m_1_type_definitions,          only: dr, sr, di, si
  use m_2_mapping_rows_to_processes, only: mapping_rows_to_processes 
  use m_5_matrix_function,           only: matrix_function
  use mpi

  implicit none

  contains

  subroutine distribute_the_matrix( n,                      &     !   1.
                                    mb,                     &     !   2.
                                    comm,                   &     !   3.
                                    box_size,               &     !   4.
                                    box_num_full,           &     !   5.
                                    box_size_last,          &     !   6.
                                    procs_row_alpha,        &     !   7.
                                    procs_row_beta,         &     !   8.
                                    procs_row_gamma,        &     !   9.
                                    num_procs_row_alpha,    &     !  10.
                                    num_procs_row_beta,     &     !  11.
                                    num_procs_row_gamma,    &     !  12.
                                    my_row,                 &     !  13.
                                    my_col,                 &     !  14.
                                    a,                      &     !  15.
                                    matrix_function )             !  16.


  !  1. interface variables

  implicit none
 
  integer(kind=si),                            intent(in)    :: n                     !   1.
  integer(kind=si),                            intent(in)    :: mb                    !   2.
  integer(kind=si),                            intent(in)    :: comm                  !   3.
  integer(kind=si),                            intent(in)    :: box_size              !   4.
  integer(kind=si),                            intent(in)    :: box_num_full          !   5.
  integer(kind=si),                            intent(in)    :: box_size_last         !   6.
  integer(kind=si),                            intent(in)    :: procs_row_alpha       !   7.
  integer(kind=si),                            intent(in)    :: procs_row_beta        !   8.
  integer(kind=si),                            intent(in)    :: procs_row_gamma       !   9.
  integer(kind=si), allocatable, dimension(:), intent(in)    :: num_procs_row_alpha   !  10.
  integer(kind=si),                            intent(in)    :: num_procs_row_beta    !  11.
  integer(kind=si), allocatable, dimension(:), intent(in)    :: num_procs_row_gamma   !  12.
  integer(kind=si),                            intent(in)    :: my_row                !  13.
  integer(kind=si),                            intent(in)    :: my_col                !  14.
  real(kind=dr), allocatable, dimension(:,:),  intent(inout) :: a                     !  15.
  external                                                      matrix_function       !  16.
  real(kind=dr)                                              :: matrix_function       !  17.

  !  2. local variables

  integer(kind=si)                           :: ierr
  integer(kind=si)                           :: i
  integer(kind=si)                           :: j 
  integer(kind=si)                           :: k
  integer(kind=si)                           :: jk
  integer(kind=si)                           :: ib 
  integer(kind=si)                           :: jb
  integer(kind=si)                           :: i_tmp
  real(kind=dr), allocatable, dimension(:,:) :: a_matrix

  !  3. building the distributed to process matrices
 
  !  4. --> part --> alpha
 
  do i = 1, procs_row_alpha

    if (my_row == num_procs_row_alpha(i) .and. my_col == 0) then

      jk = 0

      do j = 1, box_num_full+1
        do k = 1, mb     

          i_tmp = mb*my_row+k+(j-1)*box_size 
          jk = jk + 1

          allocate(a_matrix(i_tmp:i_tmp,1:n))     ! allocation of the matrix - a part of it

          do ib = i_tmp, i_tmp                    ! creation of the matrix
            do jb = 1, n
 
              a_matrix(ib,jb) = matrix_function(ib,jb)

            end do
          end do

          a(jk,:) = a_matrix(i_tmp, :)            ! assignment of the matrix

          deallocate(a_matrix)                    ! deallocation of the matrix 

        end do
      end do

    end if

  end do

  call mpi_barrier(comm, ierr)

  !  5. --> part --> beta
  
  do i = 1, procs_row_beta

    if (my_row == num_procs_row_beta .and. my_col == 0) then

      jk = 0

      !  6. --> subpart --> a

      do j = 1, box_num_full
        do k = 1, mb
       
          i_tmp = mb*my_row+k+(j-1)*box_size 
          jk = jk + 1

          allocate(a_matrix(i_tmp:i_tmp,1:n))  ! allocation of the matrix - apart of it

          do ib = i_tmp, i_tmp                 ! creation of the matrix
            do jb = 1, n
 
              a_matrix(ib,jb) = matrix_function(ib,jb)

            end do
          end do

          a(jk,:) = a_matrix(i_tmp, :)         ! assigning the matrix to local sub matrices

          deallocate(a_matrix)                 ! deallocate the matrix

        end do
      end do

      !  7. --> subpart --> b
      
      do k = 1, mod(box_size_last, mb)
        
        jk = jk + 1
        i_tmp = mb*my_row+k+box_num_full*box_size

        allocate(a_matrix(i_tmp:i_tmp,1:n))    ! allocate the matrix

        do ib = i_tmp, i_tmp                   ! create the matrix
          do jb = 1, n

              a_matrix(ib,jb) = matrix_function(ib,jb)

          end do
        end do

        a(jk,:) = a_matrix(i_tmp, :)           ! assign the matrix to local matrices
 
        deallocate(a_matrix)                   ! deallocate the matrix

      end do

    end if

  end do

  call mpi_barrier(comm, ierr)

  !  8. --> part --> gamma

  do i = 1, procs_row_gamma

    if (my_row == num_procs_row_gamma(i) .and. my_col == 0) then

      jk = 0

      do j = 1, box_num_full
        do k = 1, mb
      
          jk = jk + 1
          i_tmp = mb*my_row+k+(j-1)*box_size

          allocate(a_matrix(i_tmp:i_tmp,1:n))  ! allocate the matrix
          

          do ib = i_tmp, i_tmp                 ! create the matrix
            do jb = 1, n
     
              a_matrix(ib,jb) = matrix_function(ib,jb)

            end do
          end do

          a(jk,:) = a_matrix(i_tmp, :)         ! assign the matrix

          deallocate(a_matrix)                 ! deallocate the matrix

        end do
      end do

    end if

  end do

  call mpi_barrier(comm, ierr)

  end subroutine distribute_the_matrix

  end module m_3_distribute_the_matrix
  
!=============!
! end of code !
!=============!
