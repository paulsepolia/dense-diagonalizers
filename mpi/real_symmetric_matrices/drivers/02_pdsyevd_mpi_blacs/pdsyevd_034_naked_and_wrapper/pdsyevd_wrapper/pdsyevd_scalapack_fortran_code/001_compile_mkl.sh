#!/bin/bash

# 1. compiling. 

  mpiifort  -mkl=cluster -static_mpi           \
            -warn all                          \
            m_1_type_definitions.f90           \
            m_2_mapping_rows_to_processes.f90  \
            m_5_matrix_function.f90            \
            m_3_distribute_the_matrix.f90      \
            m_4_pdsyevd_pgg.f90                \
            driver_pdsyevd_mpi.f90             \
            -o x_pdsyevd_mkl_w

# 2. cleaning.

  rm *.mod
  rm *.o

# 3. exiting.
