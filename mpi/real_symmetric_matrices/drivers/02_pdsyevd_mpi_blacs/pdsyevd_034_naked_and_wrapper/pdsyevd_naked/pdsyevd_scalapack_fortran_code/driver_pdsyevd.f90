!==============================================================================
!==============================================================================
!
!      SUBROUTINE PDSYEVD( JOBZ, UPLO, N, A, IA, JA, DESCA, W, Z, IZ, JZ, &
!                          DESCZ, WORK, LWORK, IWORK, LIWORK, INFO )
!
!  -- ScaLAPACK routine (version 1.7) --
!     University of Tennessee, Knoxville, Oak Ridge National Laboratory,
!     and University of California, Berkeley.
!     March 14, 2000
!
!     .. Scalar Arguments ..
!     CHARACTER          JOBZ, UPLO
!     INTEGER            IA, INFO, IZ, JA, JZ, LIWORK, LWORK, N
!     ..
!     .. Array Arguments ..
!     INTEGER            DESCA( * ), DESCZ( * ), IWORK( * )
!     DOUBLE PRECISION   A( * ), W( * ), WORK( * ), Z( * )
!     ..
!
!  Purpose
!  =======
!
!  PDSYEVD computes  all the eigenvalues and eigenvectors
!  of a real symmetric matrix A by calling the recommended sequence
!  of ScaLAPACK routines.
!
!  In its present form, PDSYEVD assumes a homogeneous system and makes
!  no checks for consistency of the eigenvalues or eigenvectors across
!  the different processes.  Because of this, it is possible that a
!  heterogeneous system may return incorrect results without any error
!  messages.
!
!  Arguments
!  =========
!
!     NP = the number of rows local to a given process.
!     NQ = the number of columns local to a given process.
!
!  JOBZ    (input) CHARACTER*1
!          = 'N':  Compute eigenvalues only;     (NOT IMPLEMENTED YET)
!          = 'V':  Compute eigenvalues and eigenvectors.
!
!  UPLO    (global input) CHARACTER*1
!          Specifies whether the upper or lower triangular part of the
!          symmetric matrix A is stored:
!          = 'U':  Upper triangular
!          = 'L':  Lower triangular
!
!  N       (global input) INTEGER
!          The number of rows and columns to be operated on, i.e. the
!          order of the distributed submatrix sub( A ). N >= 0.
!
!  A       (local input/workspace) block cyclic DOUBLE PRECISION array,
!          global dimension (N, N), local dimension ( LLD_A,
!          LOCc(JA+N-1) )
!          On entry, the symmetric matrix A.  If UPLO = 'U', only the
!          upper triangular part of A is used to define the elements of
!          the symmetric matrix.  If UPLO = 'L', only the lower
!          triangular part of A is used to define the elements of the
!          symmetric matrix.
!          On exit, the lower triangle (if UPLO='L') or the upper
!          triangle (if UPLO='U') of A, including the diagonal, is
!          destroyed.
!
!  IA      (global input) INTEGER
!          A's global row index, which points to the beginning of the
!          submatrix which is to be operated on.
!
!  JA      (global input) INTEGER
!          A's global column index, which points to the beginning of
!          the submatrix which is to be operated on.
!
!  DESCA   (global and local input) INTEGER array of dimension DLEN_.
!          The array descriptor for the distributed matrix A.
!
!  W       (global output) DOUBLE PRECISION array, dimension (N)
!          If INFO=0, the eigenvalues in ascending order.
!
!  Z       (local output) DOUBLE PRECISION array,
!          global dimension (N, N),
!          local dimension ( LLD_Z, LOCc(JZ+N-1) )
!          Z contains the orthonormal eigenvectors
!          of the symmetric matrix A.
!
!  IZ      (global input) INTEGER
!          Z's global row index, which points to the beginning of the
!          submatrix which is to be operated on.
!
!  JZ      (global input) INTEGER
!          Z's global column index, which points to the beginning of
!          the submatrix which is to be operated on.
!
!  DESCZ   (global and local input) INTEGER array of dimension DLEN_.
!          The array descriptor for the distributed matrix Z.
!          DESCZ( CTXT_ ) must equal DESCA( CTXT_ )
!
!  WORK    (local workspace/output) DOUBLE PRECISION array,
!          dimension (LWORK)
!          On output, WORK(1) returns the workspace required.
!
!  LWORK   (local input) INTEGER
!          LWORK >= MAX( 1+6*N+2*NP*NQ, TRILWMIN ) + 2*N
!          TRILWMIN = 3*N + MAX( NB*( NP+1 ), 3*NB )
!          NP = NUMROC( N, NB, MYROW, IAROW, NPROW )
!          NQ = NUMROC( N, NB, MYCOL, IACOL, NPCOL )
!
!          If LWORK = -1, the LWORK is global input and a workspace
!          query is assumed; the routine only calculates the minimum
!          size for the WORK array.  The required workspace is returned
!          as the first element of WORK and no error message is issued
!          by PXERBLA.
!
!  IWORK   (local workspace/output) INTEGER array, dimension (LIWORK)
!          On exit, if LIWORK > 0, IWORK(1) returns the optimal LIWORK.
!
!  LIWORK  (input) INTEGER
!          The dimension of the array IWORK.
!          LIWORK = 7*N + 8*NPCOL + 2
!
!  INFO    (global output) INTEGER
!          = 0:  successful exit
!          < 0:  If the i-th argument is an array and the j-entry had
!                an illegal value, then INFO = -(i*100+j), if the i-th
!                argument is a scalar and had an illegal value, then
!                INFO = -i.
!          > 0:  The algorithm failed to compute the INFO/(N+1) th
!                eigenvalue while working on the submatrix lying in
!                global rows and columns mod(INFO,N+1).
!
!  Alignment requirements
!  ======================
!
!  The distributed submatrices sub( A ), sub( Z ) must verify
!  some alignment properties, namely the following expression
!  should be true:
!  ( MB_A.EQ.NB_A.EQ.MB_Z.EQ.NB_Z .AND. IROFFA.EQ.ICOFFA .AND.
!    IROFFA.EQ.0 .AND.IROFFA.EQ.IROFFZ. AND. IAROW.EQ.IZROW)
!    with IROFFA = MOD( IA-1, MB_A )
!     and ICOFFA = MOD( JA-1, NB_A ).
!
!  Further Details
!  ======= =======
!
!  Contributed by Francoise Tisseur, University of Manchester.
!
!  Reference:  F. Tisseur and J. Dongarra, "A Parallel Divide and
!              Conquer Algorithm for the Symmetric Eigenvalue Problem
!              on Distributed Memory Architectures",
!              SIAM J. Sci. Comput., 6:20 (1999), pp. 2223--2236.
!              (see also LAPACK Working Note 132)
!                http://www.netlib.org/lapack/lawns/lawn132.ps
!
!==============================================================================
!==============================================================================

  program pdsyevd_driver_mpi

  use mpi
  use m_1_type_definitions
  use m_2_mapping_rows_to_processes
  use m_3_distribute_the_matrix

  implicit none

  !  1. mpi and blacs related variables

  integer(kind=si) :: ierr        ! mpi, error flag
  integer(kind=si) :: comm        ! mpi, mpi communicator, same as 'ctxt'
  integer(kind=si) :: my_rank     ! blacs, the current acting mpi/blacs process
  integer(kind=si) :: my_rank_mpi ! mpi, same as 'my_rank'
  integer(kind=si) :: n_procs     ! mpi/blacs, total mpi processes
  integer(kind=si) :: ctxt        ! blacs, blacs communicator
  integer(kind=si) :: my_row      ! blacs, 0 <= my_row < np_row
  integer(kind=si) :: my_col      ! blacs, fixed to 0 here
  integer(kind=si) :: np_row      ! blacs, total mpi processes
  integer(kind=si) :: np_col      ! blacs, fixed to 1 here
  integer(kind=si) :: n_loc_row   ! blacs, varies
  integer(kind=si) :: n_loc_col   ! blacs, fixed here
  integer(kind=si) :: numroc      ! blacs routine

  !  2. scalapack routine related variables

  character         :: jobz       ! scalapack pdsyevd
  character         :: uplo       ! scalapack pdsyevd
  integer(kind=si)  :: info       ! scalapack descinit and pdsyevd
  integer(kind=si)  :: lwork      ! scalapack pdsyevd
  integer(kind=si)  :: liwork     ! scalapack pdsyevd
  integer(kind=si)  :: n          ! matrix dimension, scalapack pdsyevd
  integer(kind=si)  :: ia         ! scalapack pdsyevd
  integer(kind=si)  :: ja         ! scalapack pdsyevd
  integer(kind=si)  :: iz         ! scalapack pdsyevd
  integer(kind=si)  :: jz         ! scalapack pdsyevd
  integer(kind=si)  :: np         ! scalapack pdsyevd
  integer(kind=si)  :: nq         ! scalapack pdsyevd
  integer(kind=si)  :: trilwmin   ! scalapack pdsyevd
  real(kind=dr)   , allocatable, dimension(:,:) :: a      ! scalapack pdsyevd
  real(kind=dr)   , allocatable, dimension(:,:) :: z      ! scalapack pdsyevd
  real(kind=dr)   , allocatable, dimension(:)   :: w      ! scalapack pdsyevd
  real(kind=dr)   , allocatable, dimension(:)   :: work   ! scalapack pdsyevd
  real(kind=dr)   , allocatable, dimension(:)   :: iwork  ! scalapack pdsyevd
  integer(kind=si), allocatable, dimension(:)   :: desca  ! scalapack pdsyevd
  integer(kind=si), allocatable, dimension(:)   :: descz  ! scalapack pdsyevd

  !  3. local and help variables

  integer(kind=si)  :: i     ! iterator
  integer(kind=si)  :: ib    ! iterator
  integer(kind=si)  :: i_tmp ! iterator
  integer(kind=si)  :: j     ! iterator
  integer(kind=si)  :: k     ! iterator
  integer(kind=si)  :: jk    ! iterator
  integer(kind=si)  :: jb    ! iterator
  integer(kind=si)  :: nb    ! scalapack/blacs, nb = mb here
  integer(kind=si)  :: mb    ! scalapack/blacs, process grid dimension
  integer(kind=si)  :: lead_dim            ! same as 'num_loc_row'
  integer(kind=si)  :: box_size            ! size of the full processes grid box
  integer(kind=si)  :: box_num_full        ! number of total full processes grid box
  integer(kind=si)  :: box_size_last       ! size of the remaining grid box
  integer(kind=si)  :: procs_box_last_full ! num of full processes in above box
  integer(kind=si)  :: procs_row_alpha     ! num of mpi processes of type alpha
  integer(kind=si)  :: procs_row_beta      ! num of mpi processes of type beta
  integer(kind=si)  :: procs_row_gamma     ! num of mpi processes of type gamma
  integer(kind=si)  :: num_procs_row_beta  ! specific num of process beta type
  integer(kind=si), allocatable, dimension(:)   :: num_procs_row_alpha ! same as above for alpha
  integer(kind=si), allocatable, dimension(:)   :: num_procs_row_gamma ! same as above for gamma
  real(kind=dr)   , allocatable, dimension(:,:) :: a_common ! local variable to hold the matrix

  !  4. setting the variables

  !==============================================================================!
  ! Start of interface.                                                          !
  !==============================================================================!
  !  4.a. --> This interface to PDSYEVD works under no limitations.              !                
  !==============================================================================!

  n        =   64000   ! size of the matrix
  np_row   =     640   ! number of row process    --> the process grid
  jobz     =     'V'   ! 'V' or 'N'
  uplo     =     'U'   ! fixed

  !==============================================================================!
  ! 4.b. --> Fixed part of interface.                                            !
  !==============================================================================!

  mb     = n/np_row  ! fixed 
  nb     = mb        ! fixed
  np_col = 1         ! fixed 
  ia     = 1         ! fixed
  ja     = 1         ! fixed
  iz     = 1         ! fixed
  jz     = 1         ! fixed
 
  !==============================================================================!
  ! 4.c. --> End of interface.                                                   !
  !==============================================================================!

  !  5. mpi start up

  call mpi_init(ierr)

  comm = mpi_comm_world                                  

  call mpi_comm_rank(comm, my_rank_mpi, ierr)

  !  6. blacs start up

  call blacs_pinfo(my_rank, n_procs)                        ! get 'my_rank' and 'n_procs'

  call sl_init(ctxt, np_row, np_col)                        ! initialize the process grid

  call blacs_gridinfo(ctxt, np_row, np_col, my_row, my_col) ! identify each process's coordinates

  call mpi_barrier(comm, ierr)                              ! synchronization

  !  7. initialization of the interface variables

  !====================================================================!
  !  LWORK:   (local input) INTEGER                                    !
  !           LWORK >= MAX( 1+6*N+2*NP*NQ, TRILWMIN ) + 2*N            !
  !           TRILWMIN = 3*N + MAX( NB*( NP+1 ), 3*NB )                !
  !           NP = NUMROC( N, NB, MYROW, IAROW, NPROW )                !
  !           NQ = NUMROC( N, NB, MYCOL, IACOL, NPCOL )                !
  !====================================================================!

  ! 'black-box' material from scalapack user guide
  
  !  7.a.

  np       = numroc(n, nb, my_row, my_rank, np_row)
  nq       = numroc(n, mb, my_col, my_rank, np_col)
  trilwmin = 3*n + max(nb*(np+1), 3*nb)
  lwork    = max(1+6*n+2*np*nq, trilwmin) + 2*n

  !===============================================!
  !  LIWORK:  (input) INTEGER                     !
  !           The dimension of the array IWORK.   !
  !           LIWORK = 7*N + 8*NPCOL + 2          !
  !===============================================!

  ! 'black-box' material from scalapack user guide

  liwork = 7*n + 8*np_col + 2

  !  7.b.

  z = 0.0_dr
  a = 0.0_dr

  allocate(desca(1:9))
  allocate(descz(1:9))  

  call mpi_barrier(comm, ierr)

  allocate(w(1:n))
  allocate(work(1:lwork))
  allocate(iwork(1:liwork))

  z = 0.0_dr
  a = 0.0_dr

  !=================================================================!
  !=================================================================!
  ! The matrix under diagonalization.                               !
  ! I present it here so as to its form be easily understood.       !
  ! The matrix in parctice is created row by row                    !
  ! by each mpi process in step number 18.                          !
  ! I create and assign the matrix in a row by row way              !
  ! just make use of a small amount of RAM.                         !
  !=================================================================!
  !  8. building a test the matrix                                  !
  !                                                                 !
  !  allocate(a_common(1:n,1:n))                                    !
  !                                                                 !
  !  do i = 1, n                                                    !
  !    do j = 1, n                                                  !
  !                                                                 !
  !      if (abs(i - j) < 5) then                                   !
  !        a_common(i,j) = i + j                                    !
  !      else if (abs(i - j) >= 5) then                             !
  !        a_common(i,j) = 0.0_dr                                   !
  !      end if                                                     !
  !                                                                 !
  !    end do                                                       !
  !  end do                                                         !
  !                                                                 !
  !=================================================================!
  !=================================================================!

  call mpi_barrier(comm, ierr)

  !  9. mapping rows to processes

  call mapping_rows_to_processes( n,                        &   !   1.
                                  mb,                       &   !   2.
                                  nb,                       &   !   3.
                                  np_row,                   &   !   4.
                                  comm,                     &   !   5.
                                  ctxt,                     &   !   6.
                                  box_size,                 &   !   7.
                                  box_num_full,             &   !   8.
                                  box_size_last,            &   !   9.
                                  procs_box_last_full,      &   !  10.
                                  procs_row_alpha,          &   !  11.
                                  procs_row_beta,           &   !  12.
                                  procs_row_gamma,          &   !  13.
                                  num_procs_row_alpha,      &   !  14.
                                  num_procs_row_beta,       &   !  15.
                                  num_procs_row_gamma,      &   !  16.
                                  my_row,                   &   !  17.
                                  n_loc_col,                &   !  18.
                                  n_loc_row,                &   !  19.
                                  lead_dim,                 &   !  20.
                                  a,                        &   !  21.
                                  z,                        &   !  22.
                                  desca,                    &   !  23.
                                  descz )                       !  24.


  call mpi_barrier(comm, ierr)

  ! 10. local build and distribute the matrix

  call distribute_the_matrix( procs_row_alpha,        &     !   1.
                              procs_row_beta,         &     !   2.
                              procs_row_gamma,        &     !   3.
                              num_procs_row_alpha,    &     !   4.
                              num_procs_row_beta,     &     !   5.
                              num_procs_row_gamma,    &     !   6.
                              np_row,                 &     !   7.
                              lead_dim,               &     !   8.
                              my_row,                 &     !   9.
                              my_col,                 &     !  10.
                              comm,                   &     !  11.
                              box_num_full,           &     !  12.                         
                              box_size,               &     !  13.
                              box_size_last,          &     !  14.
                              procs_box_last_full,    &     !  15.
                              a,                      &     !  16.
                              n,                      &     !  17.
                              mb,                     &     !  18.
                              nb )                          !  19.

  call mpi_barrier(comm, ierr)

  ! 11. the main diagonalization step

  call pdsyevd( jobz,     &   !  1.
                uplo,     &   !  2.
                   n,     &   !  3.
                   a,     &   !  4.
                  ia,     &   !  5.
                  ja,     &   !  6.
               desca,     &   !  7.
                   w,     &   !  8.
                   z,     &   !  9.
                  iz,     &   ! 10.
                  jz,     &   ! 11.
               descz,     &   ! 12.
                work,     &   ! 13.
               lwork,     &   ! 14.
               iwork,     &   ! 15.
              liwork,     &   ! 16.
                info)         ! 17.
     
  call mpi_barrier(comm, ierr)

  ! 12. write down the info. if the job was successful or not
  
  if (my_rank == 0) then
  
    write(*,*) info
  
  end if

  ! 13. write in to the hard disk the eigevalues in asceding order

  if (my_rank == 0) then

    do i = 1, n

      write(*,*) i, w(i)

    end do
  
  end if

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! 14. must put here a write statement for the eigenvectors. !
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  ! 15. exiting blacs grid and finalize mpi here

  call blacs_gridexit(ctxt)
  call blacs_exit(0)

  end program pdsyevd_driver_mpi

!======!
! FINI !
!======!
