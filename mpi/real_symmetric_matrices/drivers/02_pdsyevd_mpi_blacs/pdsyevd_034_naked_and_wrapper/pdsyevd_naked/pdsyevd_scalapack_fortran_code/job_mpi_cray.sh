#!/bin/bash --login
#
# Parallel script produced by bolt
#        Resource: HECToR (Cray XE6 (32-core per node))
#    Batch system: PBSPro
#
# bolt is written by EPCC (http://www.epcc.ed.ac.uk)
#
#PBS -l mppwidth=640
#PBS -l mppnppn=32
#PBS -N 64k_640_cray
#PBS -A z03
#PBS -l walltime=12:00:00

# Switch to current working directory

cd $PBS_O_WORKDIR

# Run the parallel program

  export OMP_NUM_THREADS=1

aprun -n 640 -N 32 -S 8 ./x_pdsyevd_cray


