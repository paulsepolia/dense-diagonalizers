
  module m_3_distribute_the_matrix

  use m_1_type_definitions
  use m_2_mapping_rows_to_processes
  use mpi

  implicit none

  contains

  subroutine distribute_the_matrix( procs_row_alpha,        &
                                    procs_row_beta,         &
                                    procs_row_gamma,        &
                                    num_procs_row_alpha,    &
                                    num_procs_row_beta,     &
                                    num_procs_row_gamma,    &
                                    np_row,                 &
                                    lead_dim,               &
                                    my_row,                 &
                                    my_col,                 &
                                    comm,                   &
                                    box_num_full,           &                                    
                                    box_size,               & 
                                    box_size_last,          &
                                    procs_box_last_full,    &
                                    a,                      &
                                    n,                      &
                                    mb,                     &
                                    nb )                      


  !  1. interface variables
 
  implicit none
 
  integer(kind=si), intent(inout)   :: box_size
  integer(kind=si), intent(inout)    :: n
  integer(kind=si), intent(inout)    :: mb 
  integer(kind=si), intent(inout)    :: nb
  integer(kind=si), intent(in)    :: np_row 
 
  integer(kind=si), intent(inout)   :: box_num_full
  integer(kind=si), intent(inout)   :: box_size_last
  integer(kind=si), intent(inout)   :: procs_box_last_full
  integer(kind=si), intent(inout)   :: procs_row_alpha
  integer(kind=si), intent(inout)   :: procs_row_beta
  integer(kind=si), intent(inout)   :: procs_row_gamma
  integer(kind=si), allocatable, dimension(:), intent(inout) :: num_procs_row_alpha
  integer(kind=si), allocatable, dimension(:), intent(inout) :: num_procs_row_gamma
  integer(kind=si), intent(inout)   :: num_procs_row_beta
  integer(kind=si), intent(inout)   :: my_row
  integer(kind=si), intent(inout)   :: my_col
  integer(kind=si), intent(inout)   :: lead_dim
  integer(kind=si), intent(inout)   :: comm

  integer(kind=si) :: ierr
  integer(kind=si) :: i, jb, ib, i_tmp, k, j, jk
  integer(kind=si) :: info
  real(kind=dr), allocatable, dimension(:,:) :: a_common

  real(kind=dr), allocatable, dimension(:,:), intent(inout) :: a      ! scalapack pdsyevd

  !  2. local variables


  !  3. building the distributed to process matrices
 
  !  3. --> part --> alpha
 
  do i = 1, procs_row_alpha

    if (my_row == num_procs_row_alpha(i) .and. my_col == 0) then

      jk = 0

      do j = 1, box_num_full+1
        do k = 1, mb     

          i_tmp = mb*my_row+k+(j-1)*box_size 
          jk = jk + 1

          allocate(a_common(i_tmp:i_tmp,1:n))     ! allocation of the matrix - a part of it

          do ib = i_tmp, i_tmp                    ! creation of the matrix
            do jb = 1, n
 
              if (abs(ib - jb) < 5) then
                a_common(ib,jb) = ib + jb
              else if (abs(ib - jb) >= 5) then
                a_common(ib,jb) = 0.0_dr
              end if

            end do
          end do

          a(jk,:) = a_common(i_tmp, :)            ! assignment of the matrix

          deallocate(a_common)                    ! deallocation of the matrix 

        end do
      end do

    end if

  end do

  call mpi_barrier(comm, ierr)

  !  3. --> part --> beta
  
  do i = 1, procs_row_beta

    if (my_row == num_procs_row_beta .and. my_col == 0) then

      jk = 0

      !  3. --> subpart --> a

      do j = 1, box_num_full
        do k = 1, mb
       
          i_tmp = mb*my_row+k+(j-1)*box_size 
          jk = jk + 1

          allocate(a_common(i_tmp:i_tmp,1:n))  ! allocation of the matrix - apart of it

          do ib = i_tmp, i_tmp                 ! creation of the matrix
            do jb = 1, n
 
              if (abs(ib - jb) < 5) then
                a_common(ib,jb) = ib + jb
              else if (abs(ib - jb) >= 5) then
                a_common(ib,jb) = 0.0_dr
              end if

            end do
          end do

          a(jk,:) = a_common(i_tmp, :)         ! assigning the matrix to local sub matrices

          deallocate(a_common)                 ! deallocate the matrix

        end do
      end do

      !  3. --> subpart --> b
      
      do k = 1, mod(box_size_last, mb)
        
        jk = jk + 1
        i_tmp = mb*my_row+k+box_num_full*box_size

        allocate(a_common(i_tmp:i_tmp,1:n))    ! allocate the matrix

        do ib = i_tmp, i_tmp                   ! create the matrix
          do jb = 1, n

            if (abs(ib - jb) < 5) then
              a_common(ib,jb) = ib + jb
            else if (abs(ib - jb) >= 5) then
              a_common(ib,jb) = 0.0_dr
            end if

          end do
        end do

        a(jk,:) = a_common(i_tmp, :)           ! assign the matrix to local matrices
 
        deallocate(a_common)                   ! deallocate the matrix

      end do

    end if

  end do

  call mpi_barrier(comm, ierr)

  !  3. --> part --> gamma

  do i = 1, procs_row_gamma

    if (my_row == num_procs_row_gamma(i) .and. my_col == 0) then

      jk = 0

      do j = 1, box_num_full
        do k = 1, mb
      
          jk = jk + 1
          i_tmp = mb*my_row+k+(j-1)*box_size

          allocate(a_common(i_tmp:i_tmp,1:n))  ! allocate the matrix
 
          do ib = i_tmp, i_tmp                 ! create the matrix
            do jb = 1, n

              if (abs(ib - jb) < 5) then
                a_common(ib,jb) = ib + jb
              else if (abs(ib - jb) >= 5) then
                a_common(ib,jb) = 0.0_dr
              end if

            end do
          end do

          a(jk,:) = a_common(i_tmp, :)         ! assign the matrix

          deallocate(a_common)                 ! deallocate the matrix

        end do
      end do

    end if

  end do

  call mpi_barrier(comm, ierr)

  end subroutine distribute_the_matrix

  end module m_3_distribute_the_matrix
  
!=============!
! end of code !
!=============!
