#!/bin/bash

  ftn -c -O3  m_1_type_definitions.f90
  ftn -c -O3  m_2_mapping_rows_to_processes.f90
  ftn -c -O3  m_3_distribute_the_matrix.f90
  ftn -c -O3  driver_pdsyevd.f90 

  ftn -O3  m_1_type_definitions.f90              \
           m_2_mapping_rows_to_processes.f90     \
           m_3_distribute_the_matrix.f90         \
           driver_pdsyevd.f90                    \
           -o x_pdsyevd_pgi

  rm *.o
  rm *.mod  
