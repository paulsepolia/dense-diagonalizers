#!/bin/bash

# 1. compiling. 

  mpiifort -O3                                  \
  -mkl=cluster                                  \
  m_1_type_definitions.f90                      \
  pdsyev_driver.f90                             \
  pdsyev_scalapack_v17.f                        \
  -o x_mpi_scalapack_mkl

# 2. cleaning.

  rm *.mod

# 3. exiting.
