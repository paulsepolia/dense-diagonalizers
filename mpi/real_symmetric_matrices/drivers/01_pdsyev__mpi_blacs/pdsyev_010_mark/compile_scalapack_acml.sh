#!/bin/bash

# 1. compiling. 

  mpiifort -O3                              \
  -mkl=cluster                              \
  m_1_type_definitions.f90                  \
  pdsyev_driver.f90                         \
  pdsyev_scalapack_v17.f                    \
  -L/opt/acml_520/ifort64_mp/lib -lacml_mp  \
  -o x_mpi_scalapack_acml

# 2. cleaning.

  rm *.mod

# 3. exiting.
