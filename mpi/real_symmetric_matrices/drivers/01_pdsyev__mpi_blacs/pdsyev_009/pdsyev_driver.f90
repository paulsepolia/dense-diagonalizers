!==============================================================================
!  Purpose
!  =======
!
!  PDSYEV computes all eigenvalues and, optionally, eigenvectors
!  of a real symmetric matrix A by calling the recommended sequence
!  of ScaLAPACK routines.
!
!  In its present form, PDSYEV assumes a homogeneous system and makes
!  no checks for consistency of the eigenvalues or eigenvectors across
!  the different processes.  Because of this, it is possible that a
!  heterogeneous system may return incorrect results without any error
!  messages.
!
!  Notes
!  =====
!  A description vector is associated with each 2D block-cyclicly dis-
!  tributed matrix.  This vector stores the information required to
!  establish the mapping between a matrix entry and its corresponding
!  process and memory location.
!
!  In the following comments, the character _ should be read as
!  "of the distributed matrix".  Let A be a generic term for any 2D
!  block cyclicly distributed matrix.  Its description vector is DESCA:
!
!  NOTATION        STORED IN      EXPLANATION
!  --------------- -------------- --------------------------------------
!  DTYPE_A(global) DESCA( DTYPE_) The descriptor type.
!  CTXT_A (global) DESCA( CTXT_ ) The BLACS context handle, indicating
!                                 the BLACS process grid A is distribu-
!                                 ted over. The context itself is glo-
!                                 bal, but the handle (the integer
!                                 value) may vary.
!  M_A    (global) DESCA( M_ )    The number of rows in the distributed
!                                 matrix A.
!  N_A    (global) DESCA( N_ )    The number of columns in the distri-
!                                 buted matrix A.
!  MB_A   (global) DESCA( MB_ )   The blocking factor used to distribute
!                                 the rows of A.
!  NB_A   (global) DESCA( NB_ )   The blocking factor used to distribute
!                                 the columns of A.
!  RSRC_A (global) DESCA( RSRC_ ) The process row over which the first
!                                 row of the matrix A is distributed.
!  CSRC_A (global) DESCA( CSRC_ ) The process column over which the
!                                 first column of A is distributed.
!  LLD_A  (local)  DESCA( LLD_ )  The leading dimension of the local
!                                 array storing the local blocks of the
!                                 distributed matrix A.
!                                 LLD_A >= MAX(1,LOCr(M_A)).
!
!  Let K be the number of rows or columns of a distributed matrix,
!  and assume that its process grid has dimension p x q.
!  LOCr( K ) denotes the number of elements of K that a process
!  would receive if K were distributed over the p processes of its
!  process column.
!  Similarly, LOCc( K ) denotes the number of elements of K that a
!  process would receive if K were distributed over the q processes of
!  its process row.
!  The values of LOCr() and LOCc() may be determined via a call to the
!  ScaLAPACK tool function, NUMROC:
!          LOCr( M ) = NUMROC( M, MB_A, MYROW, RSRC_A, NPROW ),
!          LOCc( N ) = NUMROC( N, NB_A, MYCOL, CSRC_A, NPCOL ).
!
!  Arguments
!  =========
!
!     NP = the number of rows local to a given process.
!     NQ = the number of columns local to a given process.
!
!  JOBZ    (global input) CHARACTER*1
!          Specifies whether or not to compute the eigenvectors:
!          = 'N':  Compute eigenvalues only.
!          = 'V':  Compute eigenvalues and eigenvectors.
!
!  UPLO    (global input) CHARACTER*1
!          Specifies whether the upper or lower triangular part of the
!          symmetric matrix A is stored:
!          = 'U':  Upper triangular
!          = 'L':  Lower triangular
!
!  N       (global input) INTEGER
!          The number of rows and columns of the matrix A.  N >= 0.
!
!  A       (local input/workspace) block cyclic DOUBLE PRECISION array,
!          global dimension (N, N), local dimension ( LLD_A,
!          LOCc(JA+N-1) )
!
!          On entry, the symmetric matrix A.  If UPLO = 'U', only the
!          upper triangular part of A is used to define the elements of
!          the symmetric matrix.  If UPLO = 'L', only the lower
!          triangular part of A is used to define the elements of the
!          symmetric matrix.
!
!          On exit, the lower triangle (if UPLO='L') or the upper
!          triangle (if UPLO='U') of A, including the diagonal, is
!          destroyed.
!
!  IA      (global input) INTEGER
!          A's global row index, which points to the beginning of the
!          submatrix which is to be operated on.
!
!  JA      (global input) INTEGER
!          A's global column index, which points to the beginning of
!          the submatrix which is to be operated on.
!
!  DESCA   (global and local input) INTEGER array of dimension DLEN_.
!          The array descriptor for the distributed matrix A.
!          If DESCA( CTXT_ ) is incorrect, PDSYEV cannot guarantee
!          correct error reporting.
!
!  W       (global output) DOUBLE PRECISION array, dimension (N)
!          If INFO=0, the eigenvalues in ascending order.
!
!  Z       (local output) DOUBLE PRECISION array,
!          global dimension (N, N),
!          local dimension ( LLD_Z, LOCc(JZ+N-1) )
!          If JOBZ = 'V', then on normal exit the first M columns of Z
!          contain the orthonormal eigenvectors of the matrix
!          corresponding to the selected eigenvalues.
!          If JOBZ = 'N', then Z is not referenced.
!
!  IZ      (global input) INTEGER
!          Z's global row index, which points to the beginning of the
!          submatrix which is to be operated on.
!
!  JZ      (global input) INTEGER
!          Z's global column index, which points to the beginning of
!          the submatrix which is to be operated on.
!
!  DESCZ   (global and local input) INTEGER array of dimension DLEN_.
!          The array descriptor for the distributed matrix Z.
!          DESCZ( CTXT_ ) must equal DESCA( CTXT_ )
!
!  WORK    (local workspace/output) DOUBLE PRECISION array,
!          dimension (LWORK)
!          Version 1.0:  on output, WORK(1) returns the workspace
!          needed to guarantee completion.
!          If the input parameters are incorrect, WORK(1) may also be
!          incorrect.
!
!          If JOBZ='N' WORK(1) = minimal=optimal amount of workspace
!          If JOBZ='V' WORK(1) = minimal workspace required to
!             generate all the eigenvectors.
!
!
!  LWORK   (local input) INTEGER
!          See below for definitions of variables used to define LWORK.
!          If no eigenvectors are requested (JOBZ = 'N') then
!             LWORK >= 5*N + SIZESYTRD + 1
!          where
!             SIZESYTRD = The workspace requirement for PDSYTRD
!                         and is MAX( NB * ( NP +1 ), 3 * NB )
!          If eigenvectors are requested (JOBZ = 'V' ) then
!             the amount of workspace required to guarantee that all
!             eigenvectors are computed is:
!
!             QRMEM = 2*N-2
!             LWMIN = 5*N + N*LDC + MAX( SIZEMQRLEFT, QRMEM ) + 1
!
!          Variable definitions:
!             NB = DESCA( MB_ ) = DESCA( NB_ ) =
!                  DESCZ( MB_ ) = DESCZ( NB_ )
!             NN = MAX( N, NB, 2 )
!             DESCA( RSRC_ ) = DESCA( RSRC_ ) = DESCZ( RSRC_ ) =
!                              DESCZ( CSRC_ ) = 0
!             NP = NUMROC( NN, NB, 0, 0, NPROW )
!             NQ = NUMROC( MAX( N, NB, 2 ), NB, 0, 0, NPCOL )
!             NRC = NUMROC( N, NB, MYPROWC, 0, NPROCS)
!             LDC = MAX( 1, NRC )
!             SIZEMQRLEFT = The workspace requirement for PDORMTR
!                           when it's SIDE argument is 'L'.
!
!          With MYPROWC defined when a new context is created as:
!             CALL BLACS_GET( DESCA( CTXT_ ), 0, CONTEXTC )
!             CALL BLACS_GRIDINIT( CONTEXTC, 'R', NPROCS, 1 )
!             CALL BLACS_GRIDINFO( CONTEXTC, NPROWC, NPCOLC, MYPROWC,
!                                  MYPCOLC )
!
!          If LWORK = -1, the LWORK is global input and a workspace
!          query is assumed; the routine only calculates the minimum
!          size for the WORK array.  The required workspace is returned
!          as the first element of WORK and no error message is issued
!          by PXERBLA.
!
!  INFO    (global output) INTEGER
!          = 0:  successful exit
!          < 0:  If the i-th argument is an array and the j-entry had
!                an illegal value, then INFO = -(i*100+j), if the i-th
!                argument is a scalar and had an illegal value, then
!                INFO = -i.
!          > 0:  If INFO = 1 through N, the i(th) eigenvalue did not
!                converge in DSTEQR2 after a total of 30*N iterations.
!                If INFO = N+1, then PDSYEV has detected heterogeneity
!                by finding that eigenvalues were not identical across
!                the process grid.  In this case, the accuracy of
!                the results from PDSYEV cannot be guaranteed.
!
!  Alignment requirements
!  ======================
!
!  The distributed submatrices A(IA:*, JA:*) and Z(IZ:IZ+M-1,JZ:JZ+N-1)
!  must verify some alignment properties, namely the following
!  expressions should be true:
!
!  ( MB_A.EQ.NB_A.EQ.MB_Z .AND. IROFFA.EQ.IROFFZ .AND. IROFFA.EQ.0 .AND.
!    IAROW.EQ.IZROW )
!  where
!  IROFFA = MOD( IA-1, MB_A ) and ICOFFA = MOD( JA-1, NB_A ).
!
!  =====================================================================
!
!  Version 1.4 limitations:
!     DESCA(MB_) = DESCA(NB_)
!     DESCA(M_) = DESCZ(M_)
!     DESCA(N_) = DESCZ(N_)
!     DESCA(MB_) = DESCZ(MB_)
!     DESCA(NB_) = DESCZ(NB_)
!     DESCA(RSRC_) = DESCZ(RSRC_)
!
!==============================================================================

  program pdsyev_driver_mpi

  use mpi
  use m_1_type_definitions

  implicit none

  !  1. mpi and blacs related variables

  integer(kind=si) :: ierr        ! mpi
  integer(kind=si) :: comm        ! mpi
  integer(kind=si) :: my_rank     ! blacs
  integer(kind=si) :: my_rank_mpi ! mpi
  integer(kind=si) :: n_procs     ! mpi + blacs
  integer(kind=si) :: ctxt        ! blacs
  integer(kind=si) :: my_row      ! blacs
  integer(kind=si) :: my_col      ! blacs
  integer(kind=si) :: np_row      ! blacs
  integer(kind=si) :: np_col      ! blacs
  integer(kind=si) :: n_loc_row   ! blacs
  integer(kind=si) :: n_loc_col   ! blacs
  integer(kind=si) :: numroc      ! blacs routine

  !  2. scalapack routine related variables

  character        :: jobz
  character        :: uplo
  integer(kind=si) :: info
  integer(kind=si) :: lwork
  integer(kind=si) :: n

  integer(kind=si) :: ia
  integer(kind=si) :: ja
  integer(kind=si) :: iz
  integer(kind=si) :: jz

  real(kind=dr), allocatable, dimension(:,:) :: a
  real(kind=dr), allocatable, dimension(:,:) :: z
  real(kind=dr), allocatable, dimension(:)   :: w
  real(kind=dr), allocatable, dimension(:)   :: work

  integer(kind=si), allocatable, dimension(:) :: desca
  integer(kind=si), allocatable, dimension(:) :: descz

  !  3. local variables

  integer(kind=si) :: i
  integer(kind=si) :: j
  integer(kind=si) :: i_local
  integer(kind=si) :: j_local
  integer(kind=si) :: coef

  !  4. setting the variables

  n      = 1230
  coef   = 10
  np_row = 123
  np_col = 1 
  jobz   = 'V'
  uplo   = 'U'
  ia     = 1
  ja     = 1
  iz     = 1
  jz     = 1
 
  !  5. mpi start up

  call mpi_init(ierr)

  comm = mpi_comm_world

  call mpi_comm_rank(comm, my_rank_mpi, ierr)

  !  6. blacs start up

  call blacs_pinfo(my_rank, n_procs) ! to get 'my_rank' and 'n_procs'

  call sl_init(ctxt, np_row, np_col)

  call blacs_gridinfo(ctxt, np_row, np_col, my_row, my_col)

  call mpi_barrier(comm, ierr)

  !  7. initialization of the interface variables

  n_loc_row = n / np_row
  n_loc_col = n / np_col

  lwork = 5*n*n

  !  8. allocation of the workspace

  allocate(a(1:n_loc_row,1:n_loc_col))
  allocate(w(1:n))
  allocate(work(1:lwork))
  allocate(z(1:n_loc_row,1:n_loc_col))

  z = 0.0_dr
  a = 0.0_dr

  !  9. building a test the matrix

  do i = 1, n_loc_row
    do j = 1, n_loc_col

      i_local = i + my_row * n_loc_row
      j_local = j + my_col * n_loc_col

      if (abs(i_local - j_local) < 10) then
        a(i,j) = i_local + j_local
      else if (abs(i_local - j_local) >= 10) then
        a(i,j) = 0.0_dr
      end if

     end do
   end do

   call mpi_barrier(comm, ierr)

  ! 10. the main mpi diagonalization step

  allocate(desca(1:9))
  allocate(descz(1:9))  
  
  call descinit(desca, n, n, coef, coef, 0, 0, ctxt, coef, info)

  call descinit(descz, n, n, coef, coef, 0, 0, ctxt, coef, info)
  
  call mpi_barrier(comm, ierr)

  call pdsyev( jobz,     &   !  1.
               uplo,     &   !  2.
                  n,     &   !  3.
                  a,     &   !  4.
                 ia,     &   !  5.
                 ja,     &   !  6.
              desca,     &   !  7.
                  w,     &   !  8.
                  z,     &   !  9.
                 iz,     &   ! 10.
                 jz,     &   ! 11.
              descz,     &   ! 12.
               work,     &   ! 13.
              lwork,     &   ! 14.
              info )         ! 15.
     
  ! 11. write down the info. if the job was successful or not
  
  if (my_rank == 0) then
  
    write(*,*) info
  
  end if

  ! 12. write in to the hard disk the eigevalues in asceding order

  if (my_rank == 0) then

    do i = 1, n

      write(*,*) i, w(i)

    end do
  
  end if

  ! 13. exiting blacs grid and finalize mpi here

  call blacs_gridexit(ctxt)
  call blacs_exit(0)

  end program pdsyev_driver_mpi

!======!
! FINI !
!======!
