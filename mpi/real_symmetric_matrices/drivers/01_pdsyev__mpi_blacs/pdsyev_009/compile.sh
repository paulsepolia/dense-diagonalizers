#!/bin/bash

# 1. compiling. 

  mpiifort -O3 -static_intel  \
  -mkl=cluster                \
  m_1_type_definitions.f90    \
  pdsyev_driver.f90           \
  -o x_mpi

# 2. cleaning.

  rm *.mod

# 3. exiting.
