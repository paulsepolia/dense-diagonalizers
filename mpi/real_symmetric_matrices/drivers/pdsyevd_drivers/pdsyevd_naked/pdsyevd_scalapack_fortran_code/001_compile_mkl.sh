#!/bin/bash

# 1. compiling. 

  mpiifort  -O3 -mkl=cluster -static_mpi        \
            m_1_type_definitions.f90            \
            m_2_mapping_rows_to_processes.f90   \
            m_3_distribute_the_matrix.f90       \
            driver_pdsyevd.f90                  \
            -o x_pdsyevd_mkl

# 2. cleaning.

  rm *.mod
  rm *.o

# 3. exiting.
