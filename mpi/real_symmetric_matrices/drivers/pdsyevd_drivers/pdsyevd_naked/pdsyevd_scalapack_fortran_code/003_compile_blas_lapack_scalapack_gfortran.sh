#!/bin/bash

# 1. compiling. 

  mpif90    -O3                                     \
            m_1_type_definitions.f90                \
            m_2_mapping_rows_to_processes.f90       \
            m_3_distribute_the_matrix.f90           \
            driver_pdsyevd.f90                      \
            ../../../libs/libscalapack_gfortran.a   \
            ../../../libs/liblapack_gfortran.a      \
            ../../../libs/libblas_gfortran.a        \
            -o x_pdsyevd_gfortran

# 2. cleaning.

  rm *.mod
  rm *.o

# 3. exiting.
