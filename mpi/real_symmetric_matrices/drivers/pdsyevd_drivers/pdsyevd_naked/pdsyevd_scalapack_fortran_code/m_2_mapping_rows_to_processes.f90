  
  module m_2_mapping_rows_to_processes

  use m_1_type_definitions
  use mpi

  implicit none

  contains

  subroutine mapping_rows_to_processes( n,                        & 
                                        mb,                       &
                                        nb,                       &      
                                        np_row,                   &
                                        comm,                     &
                                        ctxt,                     &
                                        box_size,                 &
                                        box_num_full,             &
                                        box_size_last,            &
                                        procs_box_last_full,      &
                                        procs_row_alpha,          &
                                        procs_row_beta,           &
                                        procs_row_gamma,          &
                                        num_procs_row_alpha,      &
                                        num_procs_row_beta,       &
                                        num_procs_row_gamma,      &
                                        my_row,                   &
                                        n_loc_col,                &
                                        n_loc_row,                &
                                        lead_dim,                 &  
                                        a,                        &
                                        z,                        &
                                        desca,                    &
                                        descz )

  implicit none

  integer(kind=si), intent(out)   :: box_size
  integer(kind=si), intent(in)    :: n
  integer(kind=si), intent(in)    :: mb
  integer(kind=si), intent(in)    :: nb
  integer(kind=si), intent(in)    :: np_row 
 
  integer(kind=si), intent(out)   :: box_num_full
  integer(kind=si), intent(out)   :: box_size_last
  integer(kind=si), intent(out)   :: procs_box_last_full
  integer(kind=si), intent(out)   :: procs_row_alpha
  integer(kind=si), intent(out)   :: procs_row_beta
  integer(kind=si), intent(out)   :: procs_row_gamma
  integer(kind=si), allocatable, dimension(:), intent(out) :: num_procs_row_alpha
  integer(kind=si), allocatable, dimension(:), intent(out) :: num_procs_row_gamma
  integer(kind=si), intent(out)   :: num_procs_row_beta
  integer(kind=si), intent(inout) :: my_row
  integer(kind=si), intent(out)   :: n_loc_row
  integer(kind=si), intent(inout) :: n_loc_col
  integer(kind=si), intent(inout) :: lead_dim
  integer(kind=si), intent(inout) :: ctxt
  integer(kind=si), intent(inout) :: comm
  integer(kind=si) :: ierr
  integer(kind=si) :: i
  integer(kind=si) :: info
  real(kind=dr), allocatable, dimension(:,:), intent(inout) :: a      ! scalapack pdsyevd
  real(kind=dr), allocatable, dimension(:,:), intent(inout) :: z      ! scalapack pdsyevd
  integer(kind=si), allocatable, dimension(:), intent(inout)   :: desca  ! scalapack pdsyevd
  integer(kind=si), allocatable, dimension(:), intent(inout)   :: descz  ! scalapack pdsy


  !  1. the size of the box
 
  box_size = np_row * mb

  if (box_size > n) then
  
    write(*,*) " box_size > n ==> stop. adjust the 'np_row' and/or 'mb' "
 
    stop    
 
  end if

  call mpi_barrier(comm, ierr)

  !  2. how many boxes are full of small boxes <==> mpi process 

  box_num_full = n / box_size

  !  3. what is the size of the last non-full box

  box_size_last = mod(n, box_size)

  !  4. how many processes are full in the last box

  procs_box_last_full = box_size_last / mb

  !  5. how many processes are of type: alpha 

  procs_row_alpha = procs_box_last_full

  call mpi_barrier(comm, ierr)

  ! 14. about processes and rows of type: alpha

  if (procs_row_alpha /= 0) then

    allocate(num_procs_row_alpha(1:procs_row_alpha))

    do i = 1, procs_row_alpha

      num_procs_row_alpha(i) = i-1

    end do

  end if

  ! 15. about processes and rows of type: beta

  if (mod(box_size_last,mb) == 0) then
 
    procs_row_beta = 0 

  else 
   
    procs_row_beta = 1

    num_procs_row_beta = procs_row_alpha

  end if

  ! 16. about processes and rows of type: gamma

  procs_row_gamma = np_row - (procs_row_alpha + procs_row_beta)

  if (procs_row_gamma /= 0) then

    allocate(num_procs_row_gamma(1: procs_row_gamma))

    do i = 1, procs_row_gamma

      num_procs_row_gamma(i) = i-1 + procs_row_alpha + procs_row_beta

    end do

  end if

  call mpi_barrier(comm, ierr)

  ! 17. mapping partitioned matrix to process grid

  ! 17 --> part --> alpha

  do i = 1, procs_row_alpha

    if (my_row == num_procs_row_alpha(i)) then

      n_loc_row = (box_num_full + 1)* mb
      n_loc_col = n
      lead_dim  = n_loc_row

      allocate(a(1:n_loc_row,1:n_loc_col))
      allocate(z(1:n_loc_row,1:n_loc_col))

      call descinit(desca, n, n, mb, nb, 0, 0, ctxt, lead_dim, info)
      call descinit(descz, n, n, mb, nb, 0, 0, ctxt, lead_dim, info)

    end if

  end do

  call mpi_barrier(comm, ierr)

  ! 17 --> part --> beta

  do i = 1, procs_row_beta

    if (my_row == procs_row_alpha .and. mod(box_size_last, mb) /=0 ) then

      n_loc_row = box_num_full * mb + mod(box_size_last, mb)
      n_loc_col = n
      lead_dim  = n_loc_row

      allocate(a(1:n_loc_row,1:n_loc_col))
      allocate(z(1:n_loc_row,1:n_loc_col))

      call descinit(desca, n, n, mb, nb, 0, 0, ctxt, lead_dim, info)
      call descinit(descz, n, n, mb, nb, 0, 0, ctxt, lead_dim, info)

    end if

  end do

  call mpi_barrier(comm, ierr)

  ! 17 --> part --> gamma

  do i = 1, procs_row_gamma

    if (my_row == num_procs_row_gamma(i)) then

      n_loc_row = box_num_full * mb 
      n_loc_col = n
      lead_dim  = n_loc_row

      allocate(a(1:n_loc_row,1:n_loc_col))
      allocate(z(1:n_loc_row,1:n_loc_col))

      call descinit(desca, n, n, mb, nb, 0, 0, ctxt, lead_dim, info)
      call descinit(descz, n, n, mb, nb, 0, 0, ctxt, lead_dim, info)

    end if

  end do

  call mpi_barrier(comm, ierr)

  end subroutine mapping_rows_to_processes

  end module m_2_mapping_rows_to_processes
