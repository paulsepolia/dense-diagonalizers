!==============================================================================
!==============================================================================
!
!      SUBROUTINE PDSYEVD( JOBZ, UPLO, N, A, IA, JA, DESCA, W, Z, IZ, JZ, &
!                          DESCZ, WORK, LWORK, IWORK, LIWORK, INFO )
!
!  -- ScaLAPACK routine (version 1.7) --
!     University of Tennessee, Knoxville, Oak Ridge National Laboratory,
!     and University of California, Berkeley.
!     March 14, 2000
!
!     .. Scalar Arguments ..
!     CHARACTER          JOBZ, UPLO
!     INTEGER            IA, INFO, IZ, JA, JZ, LIWORK, LWORK, N
!     ..
!     .. Array Arguments ..
!     INTEGER            DESCA( * ), DESCZ( * ), IWORK( * )
!     DOUBLE PRECISION   A( * ), W( * ), WORK( * ), Z( * )
!     ..
!
!  Purpose
!  =======
!
!  PDSYEVD computes  all the eigenvalues and eigenvectors
!  of a real symmetric matrix A by calling the recommended sequence
!  of ScaLAPACK routines.
!
!  In its present form, PDSYEVD assumes a homogeneous system and makes
!  no checks for consistency of the eigenvalues or eigenvectors across
!  the different processes.  Because of this, it is possible that a
!  heterogeneous system may return incorrect results without any error
!  messages.
!
!  Arguments
!  =========
!
!     NP = the number of rows local to a given process.
!     NQ = the number of columns local to a given process.
!
!  JOBZ    (input) CHARACTER*1
!          = 'N':  Compute eigenvalues only;     (NOT IMPLEMENTED YET)
!          = 'V':  Compute eigenvalues and eigenvectors.
!
!  UPLO    (global input) CHARACTER*1
!          Specifies whether the upper or lower triangular part of the
!          symmetric matrix A is stored:
!          = 'U':  Upper triangular
!          = 'L':  Lower triangular
!
!  N       (global input) INTEGER
!          The number of rows and columns to be operated on, i.e. the
!          order of the distributed submatrix sub( A ). N >= 0.
!
!  A       (local input/workspace) block cyclic DOUBLE PRECISION array,
!          global dimension (N, N), local dimension ( LLD_A,
!          LOCc(JA+N-1) )
!          On entry, the symmetric matrix A.  If UPLO = 'U', only the
!          upper triangular part of A is used to define the elements of
!          the symmetric matrix.  If UPLO = 'L', only the lower
!          triangular part of A is used to define the elements of the
!          symmetric matrix.
!          On exit, the lower triangle (if UPLO='L') or the upper
!          triangle (if UPLO='U') of A, including the diagonal, is
!          destroyed.
!
!  IA      (global input) INTEGER
!          A's global row index, which points to the beginning of the
!          submatrix which is to be operated on.
!
!  JA      (global input) INTEGER
!          A's global column index, which points to the beginning of
!          the submatrix which is to be operated on.
!
!  DESCA   (global and local input) INTEGER array of dimension DLEN_.
!          The array descriptor for the distributed matrix A.
!
!  W       (global output) DOUBLE PRECISION array, dimension (N)
!          If INFO=0, the eigenvalues in ascending order.
!
!  Z       (local output) DOUBLE PRECISION array,
!          global dimension (N, N),
!          local dimension ( LLD_Z, LOCc(JZ+N-1) )
!          Z contains the orthonormal eigenvectors
!          of the symmetric matrix A.
!
!  IZ      (global input) INTEGER
!          Z's global row index, which points to the beginning of the
!          submatrix which is to be operated on.
!
!  JZ      (global input) INTEGER
!          Z's global column index, which points to the beginning of
!          the submatrix which is to be operated on.
!
!  DESCZ   (global and local input) INTEGER array of dimension DLEN_.
!          The array descriptor for the distributed matrix Z.
!          DESCZ( CTXT_ ) must equal DESCA( CTXT_ )
!
!  WORK    (local workspace/output) DOUBLE PRECISION array,
!          dimension (LWORK)
!          On output, WORK(1) returns the workspace required.
!
!  LWORK   (local input) INTEGER
!          LWORK >= MAX( 1+6*N+2*NP*NQ, TRILWMIN ) + 2*N
!          TRILWMIN = 3*N + MAX( NB*( NP+1 ), 3*NB )
!          NP = NUMROC( N, NB, MYROW, IAROW, NPROW )
!          NQ = NUMROC( N, NB, MYCOL, IACOL, NPCOL )
!
!          If LWORK = -1, the LWORK is global input and a workspace
!          query is assumed; the routine only calculates the minimum
!          size for the WORK array.  The required workspace is returned
!          as the first element of WORK and no error message is issued
!          by PXERBLA.
!
!  IWORK   (local workspace/output) INTEGER array, dimension (LIWORK)
!          On exit, if LIWORK > 0, IWORK(1) returns the optimal LIWORK.
!
!  LIWORK  (input) INTEGER
!          The dimension of the array IWORK.
!          LIWORK = 7*N + 8*NPCOL + 2
!
!  INFO    (global output) INTEGER
!          = 0:  successful exit
!          < 0:  If the i-th argument is an array and the j-entry had
!                an illegal value, then INFO = -(i*100+j), if the i-th
!                argument is a scalar and had an illegal value, then
!                INFO = -i.
!          > 0:  The algorithm failed to compute the INFO/(N+1) th
!                eigenvalue while working on the submatrix lying in
!                global rows and columns mod(INFO,N+1).
!
!  Alignment requirements
!  ======================
!
!  The distributed submatrices sub( A ), sub( Z ) must verify
!  some alignment properties, namely the following expression
!  should be true:
!  ( MB_A.EQ.NB_A.EQ.MB_Z.EQ.NB_Z .AND. IROFFA.EQ.ICOFFA .AND.
!    IROFFA.EQ.0 .AND.IROFFA.EQ.IROFFZ. AND. IAROW.EQ.IZROW)
!    with IROFFA = MOD( IA-1, MB_A )
!     and ICOFFA = MOD( JA-1, NB_A ).
!
!  Further Details
!  ======= =======
!
!  Contributed by Francoise Tisseur, University of Manchester.
!
!  Reference:  F. Tisseur and J. Dongarra, "A Parallel Divide and
!              Conquer Algorithm for the Symmetric Eigenvalue Problem
!              on Distributed Memory Architectures",
!              SIAM J. Sci. Comput., 6:20 (1999), pp. 2223--2236.
!              (see also LAPACK Working Note 132)
!                http://www.netlib.org/lapack/lawns/lawn132.ps
!
!==============================================================================
!==============================================================================

  module m_4_pdsyevd_pgg

  use mpi
  use m_1_type_definitions,          only: dr, sr, di, si 
  use m_2_mapping_rows_to_processes, only: mapping_rows_to_processes
  use m_5_matrix_function,           only: matrix_function
  use m_3_distribute_the_matrix,     only: distribute_the_matrix

  implicit none

  contains 

  subroutine pdsyevd_pgg( n,                 &    !   1.
                          np_row,            &    !   2.
                          jobz,              &    !   3.
                          uplo,              &    !   4.
                          comm,              &    !   5.
                          ctxt,              &    !   6.
                          w,                 &    !   7.
                          z,                 &    !   8.
                          matrix_function )       !   9.

  implicit none

  !  1. interface variables
 
  integer(kind=si), intent(in)                            :: n                !   1.
  integer(kind=si), intent(in)                            :: np_row           !   2.
  character, intent(in)                                   :: jobz             !   3.
  character, intent(in)                                   :: uplo             !   4.
  integer(kind=si), intent(in)                            :: comm             !   5.
  integer(kind=si), intent(in)                            :: ctxt             !   6.
  real(kind=dr), allocatable, dimension(:), intent(out)   :: w                !   7.
  real(kind=dr), allocatable, dimension(:,:), intent(out) :: z                !   8. 
  external                                                :: matrix_function  !  9a.
  real(kind=dr)                                           :: matrix_function  !  9b.

  !  2. local variables
 
  integer(kind=si)  :: ierr
  integer(kind=si)  :: my_rank
  integer(kind=si)  :: my_row
  integer(kind=si)  :: my_col
  integer(kind=si)  :: np_col
  integer(kind=si)  :: info
  integer(kind=si)  :: lwork
  integer(kind=si)  :: liwork
  integer(kind=si)  :: ia
  integer(kind=si)  :: ja
  integer(kind=si)  :: iz
  integer(kind=si)  :: jz
  integer(kind=si)  :: nb
  integer(kind=si)  :: mb
  integer(kind=si)  :: box_size
  integer(kind=si)  :: box_num_full
  integer(kind=si)  :: box_size_last
  integer(kind=si)  :: procs_box_last_full
  integer(kind=si)  :: procs_row_alpha
  integer(kind=si)  :: procs_row_beta
  integer(kind=si)  :: procs_row_gamma
  integer(kind=si)  :: num_procs_row_beta
  integer(kind=si)  :: np
  integer(kind=si)  :: nq
  integer(kind=si)  :: trilwmin
  integer(kind=si)  :: numroc  
  real(kind=dr)   , allocatable, dimension(:,:) :: a
  real(kind=dr)   , allocatable, dimension(:)   :: work
  real(kind=dr)   , allocatable, dimension(:)   :: iwork
  integer(kind=si), allocatable, dimension(:)   :: desca
  integer(kind=si), allocatable, dimension(:)   :: descz
  integer(kind=si), allocatable, dimension(:)   :: num_procs_row_alpha
  integer(kind=si), allocatable, dimension(:)   :: num_procs_row_gamma

  !  3. blacs grid info

  call blacs_gridinfo(ctxt, np_row, np_col, my_row, my_col)

  !  4. fixed variables

  mb     = n/np_row  ! fixed 
  nb     = mb        ! fixed
  np_col = 1         ! fixed 
  ia     = 1         ! fixed
  ja     = 1         ! fixed
  iz     = 1         ! fixed
  jz     = 1         ! fixed
 
  !  5. setting the working space

  !====================================================================!
  !  LWORK:   (local input) INTEGER                                    !
  !           LWORK >= MAX( 1+6*N+2*NP*NQ, TRILWMIN ) + 2*N            !
  !           TRILWMIN = 3*N + MAX( NB*( NP+1 ), 3*NB )                !
  !           NP = NUMROC( N, NB, MYROW, IAROW, NPROW )                !
  !           NQ = NUMROC( N, NB, MYCOL, IACOL, NPCOL )                !
  !====================================================================!

  np       = numroc(n, nb, my_row, my_rank, np_row)
  nq       = numroc(n, mb, my_col, my_rank, np_col)
  trilwmin = 3*n + max(nb*(np+1), 3*nb)
  lwork    = max(1+6*n+2*np*nq, trilwmin) + 2*n

  !===============================================!
  !  LIWORK:  (input) INTEGER                     !
  !           The dimension of the array IWORK.   !
  !           LIWORK = 7*N + 8*NPCOL + 2          !
  !===============================================!

  liwork = 7*n + 8*np_col + 2

  !  6.

  allocate(w(1:n))
  allocate(work(1:lwork))
  allocate(iwork(1:liwork))

  call mpi_barrier(comm, ierr)

  !  7. mapping rows to processes

  call mapping_rows_to_processes( n,                        &   !   1.
                                  mb,                       &   !   2.
                                  nb,                       &   !   3.
                                  np_row,                   &   !   4.
                                  comm,                     &   !   5.
                                  ctxt,                     &   !   6.
                                  box_size,                 &   !   7.
                                  box_num_full,             &   !   8.
                                  box_size_last,            &   !   9.
                                  procs_box_last_full,      &   !  10.
                                  procs_row_alpha,          &   !  11.
                                  procs_row_beta,           &   !  12.
                                  procs_row_gamma,          &   !  13.
                                  num_procs_row_alpha,      &   !  14.
                                  num_procs_row_beta,       &   !  15.
                                  num_procs_row_gamma,      &   !  16.
                                  my_row,                   &   !  17.
                                  a,                        &   !  18.
                                  z,                        &   !  19.
                                  desca,                    &   !  20.
                                  descz )                       !  21.

  call mpi_barrier(comm, ierr)

  !  8. distribute the global matrix to sub-matrices

  call distribute_the_matrix( n,                      &     !   1.
                              mb,                     &     !   2.
                              comm,                   &     !   3.
                              box_size,               &     !   4.
                              box_num_full,           &     !   5.
                              box_size_last,          &     !   6.
                              procs_row_alpha,        &     !   7.
                              procs_row_beta,         &     !   8.
                              procs_row_gamma,        &     !   9.
                              num_procs_row_alpha,    &     !  10.
                              num_procs_row_beta,     &     !  11.
                              num_procs_row_gamma,    &     !  12.
                              my_row,                 &     !  13.
                              my_col,                 &     !  14.
                              a,                      &     !  15.
                              matrix_function )             !  16.

  call mpi_barrier(comm, ierr)

  !  9. call the pdsyevd scalapack diagonalizer

  call pdsyevd( jobz,     &   !  1.
                uplo,     &   !  2.
                   n,     &   !  3.
                   a,     &   !  4.
                  ia,     &   !  5.
                  ja,     &   !  6.
               desca,     &   !  7.
                   w,     &   !  8.
                   z,     &   !  9.
                  iz,     &   ! 10.
                  jz,     &   ! 11.
               descz,     &   ! 12.
                work,     &   ! 13.
               lwork,     &   ! 14.
               iwork,     &   ! 15.
              liwork,     &   ! 16.
                info )        ! 17.

  call mpi_barrier(comm, ierr)

  ! 10. local deallocations

  deallocate(desca)
  deallocate(descz)  
  deallocate(work)
  deallocate(iwork)
  deallocate(a)

  end subroutine pdsyevd_pgg

  end module m_4_pdsyevd_pgg

!=============!
! end of code !
!=============!
