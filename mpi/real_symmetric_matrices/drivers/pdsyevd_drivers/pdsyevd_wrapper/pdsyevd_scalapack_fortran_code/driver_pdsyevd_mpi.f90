
  program driver_pdsyevd_mpi

  use mpi
  use m_1_type_definitions
  use m_4_pdsyevd_pgg
  use m_5_matrix_function, only: matrix_function

  implicit none

  !  1. interface variables

  integer(kind=si)                           :: n                  !   1.
  integer(kind=si)                           :: np_row             !   2.
  character                                  :: jobz               !   3.
  character                                  :: uplo               !   4.
  integer(kind=si)                           :: comm               !   5.
  integer(kind=si)                           :: ctxt               !   6.
  real(kind=dr), allocatable, dimension(:)   :: w                  !   7.
  real(kind=dr), allocatable, dimension(:,:) :: z                  !   8.
!===========================================================================!
! real(kind=dr)                              :: matrix_function    !  9a.   !
! external                                   :: matrix_function    !  9b.   !
!===========================================================================!

  !  2. local variables

  integer(kind=si) :: ierr
  integer(kind=si) :: my_rank 
  integer(kind=si) :: np_col
  integer(kind=si) :: i
  integer(kind=si) :: j
!  real(kind=dr), dimension(1) :: dimen_line
!  integer(kind=di) :: rec_len

  !  3. the interface to PDSYEVD

  n        =    12123   ! size of the matrix
  np_row   =       4   ! number of row process --> the process grid
  np_col   =       1   ! fixed. do not alter it.
  jobz     =     'V'   ! 'V' or 'N'
  uplo     =     'U'   ! fixed
 
  !  4. mpi start up

  call mpi_init(ierr)

  comm = mpi_comm_world

  call mpi_comm_rank(comm, my_rank, ierr)

  call mpi_barrier(comm, ierr)

  !  5. blacs start up

  call sl_init(ctxt, np_row, np_col)       

  call mpi_barrier(comm, ierr)

  !  6. the main diagonalization step

  call pdsyevd_pgg( n,                 &      !  1.
                    np_row,            &      !  2.
                    jobz,              &      !  3.
                    uplo,              &      !  4.
                    comm,              &      !  5.
                    ctxt,              &      !  6.
                    w,                 &      !  7.
                    z,                 &      !  8.
                    matrix_function )         !  9.

  call mpi_barrier(comm, ierr) 

  !  7. write in to the hard disk the eigevalues in asceding order

  if (my_rank == 0) then

    do i = 1, n

      write(*,*) i, w(i)

    end do
  
  end if

  call mpi_barrier(comm, ierr)

!  if (my_rank == 0) then

!    do i = 1, n
!      do j = (n/np_row)*my_rank+1, (n/np_row) + (n/np_row)*my_rank

!        write(*,*) j, i, abs(z(j-(n/np_row)*my_rank,i))

!      end do
!    end do

!  end if

!  if (my_rank == 0) then

!    do i =  1, n
!      do j = 1, n/np_row

!      write(*,*) j, i, abs(z(j,i)) 

!      end do
!    end do

!  end if

  call mpi_barrier(comm, ierr)

!===================

!  inquire( iolength = rec_len ) dimen_line

!  open( unit = 18,              & 
!        form = "unformatted",   &
!        file = "file_vectors",  &
!        status = "replace",     &
!        action = "write",       &
!        access = "direct",      &
!        recl = rec_len )    

!  call mpi_barrier(comm, ierr)

!    do i = 1, n
!      do j =1, n/(my_rank+1)     

!        write( unit=18, rec=j+(n/(my_rank+1))*my_rank+(i-1)*n )   &
        
!        z(j,i)

!      end do
!    end do

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !  8. must put here a write statement for the eigenvectors. !
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  !  9. deallocations

  deallocate(z)
  deallocate(w)

  call mpi_barrier(comm, ierr)

  ! 10. exiting blacs grid and finalize mpi here

  call blacs_gridexit(ctxt)
  call blacs_exit(0)

  end program driver_pdsyevd_mpi

!=============!
! end of code !
!=============!


  ! 11.b. writing the eigenvectors.

  ! setting the nloc.

!  nloc = n/p

!  if ( mod(n,p) /= 0 .and. my_rank < mod(n,p) ) then

!    nloc = n/p + 1

!  else if ( mod(n,p) /= 0 .and. my_rank >= mod(n,p) ) then

!    nloc = n/p

!  end if

!  call MPI_Barrier( comm, ierr )

  ! setting the common record length

!  inquire( iolength = rec_len ) dimen_line

!  open( unit = 18,              & 
!        form = "unformatted",   &
!        file = ve_file,         &
!        status = "replace",     &
!        action = "write",       &
!        access = "direct",      &
!        recl = rec_len )    

!  call MPI_Barrier( comm, ierr )

!  ! writting to the hard disk the eigenvectors 
!  ! case: mod(n,p) == 0

!  if ( mod(n,p) == 0 ) then

!    do i1 = 1, nev
!      do i2 =1, nloc     

!        write( unit=18, rec=i2+nloc*my_rank+(i1-1)*n )   &
        
!        vectors_array(i2,i1)

!      end do
!    end do

!  end if

!  call MPI_Barrier( comm, ierr )

!  ! writting to the hard disk the eigenvectors: 
!  ! case: mod(n,p) /= 0 && my_rank < mod(n,p)

!  if ( mod(n,p) /= 0 .and. my_rank < mod(n,p) ) then

!    do i1 = 1, nev
!      do i2 =1, nloc     

!        write( unit=18, rec=i2+nloc*my_rank+(i1-1)*n )   &
        
!        vectors_array(i2,i1)

!      end do
!    end do
  
!  end if

!  call MPI_Barrier( comm, ierr )

!  ! writting to the hard disk the eigenvectors: 
!  ! case: mod(n,p) /= 0 && my_rank >= mod(n,p)

!  if ( mod(n,p) /= 0 .and. my_rank >= mod(n,p) ) then

!    do i1 = 1, nev
!      do i2 =1, nloc     

!        write( unit=18, rec=i2+(nloc+1)*mod(n,p) +              & 
!                            nloc*(my_rank-mod(n,p))+(i1-1)*n )  &
        
!        vectors_array(i2,i1)

!      end do
!    end do
  
!  end if

!  ! 11.c closing the unit for the eigenvectors

!  close( unit=18, status="keep" )

!  ! 11.d mpi finalization.

!  call MPI_Finalize( ierr )

!  end program driver_arpack_coo_mpi

!========================================================================================
! FINI.
!========================================================================================
