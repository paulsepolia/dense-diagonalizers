!===================================!
! Code for the matrix to build      !
!===================================!
! Date: 2012-12-13                  !
! Author: Pavlos G. Galiatsatos     !
!===================================! 

  module m_5_matrix_function

  use m_1_type_definitions, only: dr, sr, di, si

  implicit none

  contains

  real(kind=dr) function matrix_function(i,j)
  
  implicit none  

  integer(kind=si) :: i
  integer(kind=si) :: j 
  
    if (abs(i-j) < 5) then
      matrix_function = i+j
    else if (abs(i-j) >= 5) then
      matrix_function = 0.0_dr
   end if

  end function matrix_function

  end module m_5_matrix_function

!==============!
! end of code. !
!==============!
