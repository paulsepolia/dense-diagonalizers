#!/bin/bash

# 1. compiling. 

  mpiifort -O3 -static-intel  \
  -mkl=cluster                \
  m_1_type_definitions.f90    \
  pdsyevx_driver.f90          \
  -o x_mpi_mkl

# 2. cleaning.

  rm *.mod

# 3. exiting.
