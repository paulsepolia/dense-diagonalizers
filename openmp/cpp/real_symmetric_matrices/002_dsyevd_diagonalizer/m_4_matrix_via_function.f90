!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2014/12/12              !
!===============================!

  module m_4_matrix_via_function

  use m_1_type_definitions, only: si, dr, fi
  use m_2_matrix_function,  only: matrix_function

  implicit none

  contains

!====================================================================!
!====================================================================!

  subroutine matrix_via_function(m,                &  !  1. rows          
                                 n,                &  !  2. columns
                                 a,                &  !  3. matrix
                                 matrix_function)     !  4. function

  implicit none

  !  1. Interface variables

  integer(kind=fi), intent(in)                              :: m               ! rows
  integer(kind=fi), intent(in)                              :: n               ! columns
  real(kind=dr), allocatable, dimension(:,:), intent(inout) :: a               ! matrix 
  real(kind=dr)                                             :: matrix_function ! function 

  !  2. Local variables

  integer(kind=si) :: i
  integer(kind=si) :: j
  integer(kind=si) :: chunk
  integer(kind=si) :: low_ba
  integer(kind=si) :: up_ba
  integer(kind=si) :: i_am
  integer(kind=si) :: n_threads
  integer(kind=si) :: omp_get_num_threads
  integer(kind=si) :: omp_get_thread_num

  !  3. Building a test matrix. OPTIMAL USE OF CACHE.

  !$omp parallel           &
  !$omp private(i)         &
  !$omp private(j)         &
  !$omp private(chunk)     &
  !$omp private(i_am)      &
  !$omp private(low_ba)    &
  !$omp private(up_ba)     &           
  !$omp shared(m)          &
  !$omp shared(n)          &
  !$omp shared(a)          &
  !$omp shared(n_threads)

  i_am = omp_get_thread_num()
  n_threads = omp_get_num_threads()
  chunk = int(n, kind=si) / n_threads
  low_ba = i_am * chunk + 1_si
  up_ba = (i_am + 1_si) * chunk
 
  if (i_am == n_threads-1_si) then
    up_ba = int(n, kind=si)
  end if

  do i = low_ba, up_ba 
    do j = 1_si, int(m, kind=si)

      a(j,i) = matrix_function(j,i)

    end do
  end do

  !$omp end parallel

  end subroutine matrix_via_function

!====================================================================!
!====================================================================!

  end module m_4_matrix_via_function

! FINI
