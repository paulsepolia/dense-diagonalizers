!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2014/12/13              !
!===============================!

  module m_5_dsyevx_pgg

  use m_1_type_definitions, only: dr, fi

  implicit none

  contains

!===================================================================!
!===================================================================!

  subroutine dsyevx_pgg(jobz,       &  !  1
                        range_loc,  &  !  2
                        uplo,       &  !  3
                        n,          &  !  4
                        a,          &  !  5
                        vl,         &  !  6
                        vu,         &  !  7
                        il,         &  !  8
                        iu,         &  !  9
                        abstol,     &  ! 10
                        m,          &  ! 11
                        w,          &  ! 12
                        z,          &  ! 13
                        ifail,      &  ! 14
                        info)          ! 15

  implicit none   
  
!====================================================================!
! This is a copy-paste text from LAPACK's site                       !
!====================================================================!
!
!    SUBROUTINE DSYEVX( JOBZ, RANGE, UPLO, N, A, LDA, VL, VU, IL, IU,
!     $                   ABSTOL, M, W, Z, LDZ, WORK, LWORK, IWORK,
!     $                   IFAIL, INFO )
!
!  -- LAPACK driver routine (version 3.2) --
!  -- LAPACK is a software package provided by Univ. of Tennessee,    --
!  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
!     November 2006
!
!     .. Scalar Arguments ..
!     CHARACTER          JOBZ, RANGE, UPLO
!     INTEGER            IL, INFO, IU, LDA, LDZ, LWORK, M, N
!     DOUBLE PRECISION   ABSTOL, VL, VU
!     ..
!     .. Array Arguments ..
!     INTEGER            IFAIL( * ), IWORK( * )
!     DOUBLE PRECISION   A( LDA, * ), W( * ), WORK( * ), Z( LDZ, * )
!     ..
!
!  Purpose
!  =======
!
!  DSYEVX computes selected eigenvalues and, optionally, eigenvectors
!  of a real symmetric matrix A.  Eigenvalues and eigenvectors can be
!  selected by specifying either a range of values or a range of indices
!  for the desired eigenvalues.
!
!  Arguments
!  =========
!
!  JOBZ    (input) CHARACTER*1
!          = 'N':  Compute eigenvalues only;
!          = 'V':  Compute eigenvalues and eigenvectors.
!
!  RANGE   (input) CHARACTER*1
!          = 'A': all eigenvalues will be found.
!          = 'V': all eigenvalues in the half-open interval (VL,VU]
!                 will be found.
!          = 'I': the IL-th through IU-th eigenvalues will be found.
!
!  UPLO    (input) CHARACTER*1
!          = 'U':  Upper triangle of A is stored;
!          = 'L':  Lower triangle of A is stored.
!
!  N       (input) INTEGER
!          The order of the matrix A.  N >= 0.
!
!  A       (input/output) DOUBLE PRECISION array, dimension (LDA, N)
!          On entry, the symmetric matrix A.  If UPLO = 'U', the
!          leading N-by-N upper triangular part of A contains the
!          upper triangular part of the matrix A.  If UPLO = 'L',
!          the leading N-by-N lower triangular part of A contains
!          the lower triangular part of the matrix A.
!          On exit, the lower triangle (if UPLO='L') or the upper
!          triangle (if UPLO='U') of A, including the diagonal, is
!          destroyed.
!
!  LDA     (input) INTEGER
!          The leading dimension of the array A.  LDA >= max(1,N).
!
!  VL      (input) DOUBLE PRECISION
!  VU      (input) DOUBLE PRECISION
!          If RANGE='V', the lower and upper bounds of the interval to
!          be searched for eigenvalues. VL < VU.
!          Not referenced if RANGE = 'A' or 'I'.
!
!  IL      (input) INTEGER
!  IU      (input) INTEGER
!          If RANGE='I', the indices (in ascending order) of the
!          smallest and largest eigenvalues to be returned.
!          1 <= IL <= IU <= N, if N > 0; IL = 1 and IU = 0 if N = 0.
!          Not referenced if RANGE = 'A' or 'V'.
!
!  ABSTOL  (input) DOUBLE PRECISION
!          The absolute error tolerance for the eigenvalues.
!          An approximate eigenvalue is accepted as converged
!          when it is determined to lie in an interval [a,b]
!          of width less than or equal to
!
!                  ABSTOL + EPS *   max( |a|,|b| ) ,
!
!          where EPS is the machine precision.  If ABSTOL is less than
!          or equal to zero, then  EPS*|T|  will be used in its place,
!          where |T| is the 1-norm of the tridiagonal matrix obtained
!          by reducing A to tridiagonal form.
!
!          Eigenvalues will be computed most accurately when ABSTOL is
!          set to twice the underflow threshold 2*DLAMCH('S'), not zero.
!          If this routine returns with INFO>0, indicating that some
!          eigenvectors did not converge, try setting ABSTOL to
!          2*DLAMCH('S').
!
!          See "Computing Small Singular Values of Bidiagonal Matrices
!          with Guaranteed High Relative Accuracy," by Demmel and
!          Kahan, LAPACK Working Note #3.
!
!  M       (output) INTEGER
!          The total number of eigenvalues found.  0 <= M <= N.
!          If RANGE = 'A', M = N, and if RANGE = 'I', M = IU-IL+1.
!
!  W       (output) DOUBLE PRECISION array, dimension (N)
!          On normal exit, the first M elements contain the selected
!          eigenvalues in ascending order.
!
!  Z       (output) DOUBLE PRECISION array, dimension (LDZ, max(1,M))
!          If JOBZ = 'V', then if INFO = 0, the first M columns of Z
!          contain the orthonormal eigenvectors of the matrix A
!          corresponding to the selected eigenvalues, with the i-th
!          column of Z holding the eigenvector associated with W(i).
!          If an eigenvector fails to converge, then that column of Z
!          contains the latest approximation to the eigenvector, and the
!          index of the eigenvector is returned in IFAIL.
!          If JOBZ = 'N', then Z is not referenced.
!          Note: the user must ensure that at least max(1,M) columns are
!          supplied in the array Z; if RANGE = 'V', the exact value of M
!          is not known in advance and an upper bound must be used.
!
!  LDZ     (input) INTEGER
!          The leading dimension of the array Z.  LDZ >= 1, and if
!          JOBZ = 'V', LDZ >= max(1,N).
!
!  WORK    (workspace/output) DOUBLE PRECISION array, dimension (MAX(1,LWORK))
!          On exit, if INFO = 0, WORK(1) returns the optimal LWORK.
!
!  LWORK   (input) INTEGER
!          The length of the array WORK.  LWORK >= 1, when N <= 1;
!          otherwise 8*N.
!          For optimal efficiency, LWORK >= (NB+3)*N,
!          where NB is the max of the blocksize for DSYTRD and DORMTR
!          returned by ILAENV.
!
!          If LWORK = -1, then a workspace query is assumed; the routine
!          only calculates the optimal size of the WORK array, returns
!          this value as the first entry of the WORK array, and no error
!          message related to LWORK is issued by XERBLA.
!
!  IWORK   (workspace) INTEGER array, dimension (5*N)
!
!  IFAIL   (output) INTEGER array, dimension (N)
!          If JOBZ = 'V', then if INFO = 0, the first M elements of
!          IFAIL are zero.  If INFO > 0, then IFAIL contains the
!          indices of the eigenvectors that failed to converge.
!          If JOBZ = 'N', then IFAIL is not referenced.
!
!  INFO    (output) INTEGER
!          = 0:  successful exit
!          < 0:  if INFO = -i, the i-th argument had an illegal value
!          > 0:  if INFO = i, then i eigenvectors failed to converge.
!                Their indices are stored in array IFAIL.
!
!====================================================================!
!====================================================================!

  !  1. Interface variables

  character, intent(in)                                      :: jobz
  character, intent(in)                                      :: range_loc
  character, intent(in)                                      :: uplo
  integer(kind=fi), intent(in)                               :: n
  integer(kind=fi), intent(out)                              :: info
  integer(kind=fi), intent(in)                               :: il
  integer(kind=fi), intent(in)                               :: iu
  integer(kind=fi), intent(in)                               :: m
  integer(kind=fi), allocatable, dimension(:), intent(inout) :: ifail
  real(kind=dr), intent(in)                                  :: vl
  real(kind=dr), intent(in)                                  :: vu
  real(kind=dr), intent(in)                                  :: abstol
  real(kind=dr), allocatable, dimension(:,:), intent(inout)  :: a
  real(kind=dr), allocatable, dimension(:)  , intent(inout)  :: w
  real(kind=dr), allocatable, dimension(:,:), intent(inout)  :: z 
 
  !  2. Local variables

  integer(kind=fi)                         :: ldz
  integer(kind=fi)                         :: lda
  integer(kind=fi)                         :: lwork
  real(kind=dr), allocatable, dimension(:) :: work
  real(kind=dr), allocatable, dimension(:) :: iwork

  !  3. Local variables initialization

  ldz    = n
  lda    = n
  lwork  = 35_fi*n ! (NB+3)*N=(32+3)*N=35*N, NB=32 for ACML and MKL

  !  4. Allocation of workspace

  allocate(work(1:lwork))
  allocate(iwork(1:5*n))

  !  5. The main diagonalization step

  call dsyevx(jobz,       &  !   1
              range_loc,  &  !   2
              uplo,       &  !   3
              n,          &  !   4
              a,          &  !   5
              lda,        &  !   6
              vl,         &  !   7
              vu,         &  !   8
              il,         &  !   9
              iu,         &  !  10
              abstol,     &  !  11
              m,          &  !  12
              w,          &  !  13
              z,          &  !  14
              ldz,        &  !  15
              work,       &  !  16
              lwork,      &  !  17
              iwork,      &  !  18
              ifail,      &  !  19
              info)          !  20

  !  6. local deallocations

  deallocate(work)
  deallocate(iwork)

  end subroutine dsyevx_pgg

!====================================================================!
!====================================================================!

  end module m_5_dsyevx_pgg

! FINI
