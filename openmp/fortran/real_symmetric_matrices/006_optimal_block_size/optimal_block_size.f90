!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2014/12/12              !
!===============================!

program optimal_block_size
   
  use m_1_type_definitions, only: si
 
  implicit none

  character        :: UPLO = 'U'
  integer(kind=si) :: N = 200000                   
  integer(kind=si) :: NB1
  integer(kind=si) :: NB2
  integer(kind=si) :: ILAENV

  NB1 = ILAENV(1, 'DSYTRD', UPLO, N, -1, -1, -1)
  NB2 = ILAENV(1, 'DORMTR', UPLO, N, -1, -1, -1)

  write(*,*) 'DSYTRD --> ', NB1 
  write(*,*) 'DORMTR --> ', NB2

end program optimal_block_size

! FINI
