#!/bin/bash

# 1. compile

  gfortran  -std=f2008               \
            -O3                      \
            -fopenmp                 \
            m_1_type_definitions.f90 \
            optimal_block_size.f90   \
            /opt/lapack/lib_2015/liblapack_gnu_i8.a \
            /opt/blas/lib_2015/libblas_gnu_i8.a     \
            -o x_gnu_i8

# 2. clean

  rm *.mod

# 3. exit
